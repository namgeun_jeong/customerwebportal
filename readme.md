# ABCorp Customers Web Portal



# Installation
Customer Web Portal is built in using the [Laravel](https://laravel.com/docs/5.6#server-requirements) PHP framework, you will need to make sure your server meets the following [requirements](https://laravel.com/docs/5.6#server-requirements).

Clone repository:
```sh
$ git clone https://namgeun_jeong@bitbucket.org/namgeun_jeong/customerwebportal.git .
```
Install Composer dependencies:
```sh
$ composer install
```
Install npm dependencies:
```sh
$ npm install
```
Rename **.env.example** to **.env** and populate it with appropriate config data.