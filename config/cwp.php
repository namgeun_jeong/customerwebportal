<?php

return [

    'api' => [
        'domain' => env('API_DOMAIN'),
        'endpoint' => env('API_ENDPOINT'),

        'configuration_domain' => env('API_CONFIGURATION_DOMAIN'),
        'configuration_endpoint' => env('API_CONFIGURATION_ENDPOINT'),
    ],

    'orderItemsPerPage' => 25,
    'customerOrdersPerPage' => 25,
    'workOrdersItemsPerPage' => 25,

    'analytics' => [
        'hotjar' => env('HOTJAR'),
        'google' => env('GOOGLE_ANALYTICS'),
    ]
];
