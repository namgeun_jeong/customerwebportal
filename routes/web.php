<?php



Route::get('/', function () {
    return redirect('orders/customers');
})->middleware('auth');

Route::get( '/account/callback', '\Auth0\Login\Auth0Controller@callback' )->name( 'auth0-callback' );
Route::get('/login', 'Auth\Auth0IndexController@login' )->name( 'login' );
Route::get('/logout', 'Auth\Auth0IndexController@logout' )->name( 'logout' )->middleware('auth');

Route::group(['middleware' => 'auth'], function () {

    /**
     * Users
     */
    Route::group(['namespace' => 'Administration'], function () {
        Route::group(['middleware' => 'role:Admin,ABCorp Admin'], function () {
            Route::resource('users', 'UsersController');
            Route::post('users/{id}/update-roles', 'UsersController@updateRoles')->name('users.update-roles');
            Route::post('users/{id}/password-change', 'UsersController@passwordReset')->name('users.password-reset');
        });

        Route::post('users/updateTimezone', 'UsersController@updateTimezone')->name('users.update-timezone');
    });

    /**
     * Product Management
     */
    Route::group(['namespace' => 'ProductManagement', 'middleware' => 'role:AccountManager,ABCorp Account Manager,ABCorp Admin,OrderManager,OrderUser'], function () {
        Route::resource('management', 'ProductManagementController', ['only' => ['index', 'store']]);
        Route::resource('verticals', 'VerticalsController', ['only' => ['index', 'store']]);
        Route::resource('products', 'ProductsController', ['only' => ['store']]);
    });

    /**
     * Orders
     */
    Route::group(['prefix' => 'orders', 'namespace' => 'Orders', 'middleware' => 'role:OrderUser,OrderManager,RequestUser,RequestManager,ABCorp Account Manager,ABCorp Admin'], function () {
        /**
         * Customer orders
         */
        Route::get('customers', 'CustomerOrdersController@data')->name('customers.data');
        Route::post('customers', 'CustomerOrdersController@data');

        /**
         * Rejected items
         */
        Route::get('customers/{customer_order_uid}/rejected', 'RejectedItemsController@data')->name('rejected.data');
        Route::post('customers/{customer_order_uid}/rejected', 'RejectedItemsController@data');

        /**
         * Work orders
         */
        Route::group(['prefix' => 'work'], function () {
            Route::get('customer/{customer_order_uid}', 'WorkOrdersController@data')->name('work.customer');
            Route::post('customer/{customer_order_uid}', 'WorkOrdersController@data');
            Route::get('/', 'WorkOrdersController@data')->name('work.data');
            Route::post('/', 'WorkOrdersController@data');
        });
        Route::resource('work', 'WorkOrdersController', ['only' => ['show'],
            'parameters' => [
                'work' => 'work_order_uid'
            ]
        ]);

        /**
         * Order items
         */
        Route::resource('order-items', 'OrderItemsController', [
            'only' => ['index', 'show'],
        ]);
        Route::group(['prefix' => 'order-items'], function () {
            Route::post('{item_uid}/actions', 'OrderItemsController@actions')->name('order-items.actions');
            Route::post('action', 'OrderItemsController@action')->name('order-items.action');
            Route::get('order/{work_order_uid}', 'OrderItemsController@data')->name('order-items.order');
            Route::post('order/{work_order_uid}', 'OrderItemsController@data');
        });

        /**
         * Product Items
         */
        Route::get('product-items', 'ProductItemsController@data')->name('products.data');
        Route::post('product-items', 'ProductItemsController@data');

        /**
         * Customers requests
         */
        Route::group(['middleware' => 'role:RequestUser,RequestManager,ABCorp Account Manager,ABCorp Admin', 'prefix' => 'requests'], function () {
            Route::get('', 'CustomerRequestsController@data')->name('requests.data');
            Route::post('/{uid}/cancel', 'CustomerRequestsController@cancel')->name('requests.cancel');
            Route::post('', 'CustomerRequestsController@data');
        });
        Route::group(['middleware' => 'role:RequestManager,ABCorp Account Manager, ABCorp Admin'], function () {
            Route::resource('requests', 'CustomerRequestsController', ['only' => ['edit', 'update', 'show'], 'parameters' => [
                'request' => 'request_uid'
            ]]);
        });
    });

    Route::group(['namespace' => 'Reports', 'middleware' => 'role:ReportUser,ReportManager,ABCorp Account Manager,ABCorp Admin,OrderManager,OrderUser'], function () {
        /**
         * Reports
         */
        Route::group(['middleware' => 'role:ReportManager.ABCorp Account Manager,ABCorp Admin,OrderManager,OrderUser'], function () {
            Route::group(['prefix' => 'reports'], function () {
                Route::get('{report}/schedules', 'SchedulesController@data')->name('reports.schedules');
                Route::post('{report}/schedules', 'SchedulesController@data');
                Route::get('/schedules', 'SchedulesController@data')->name('schedules.data');
                Route::post('/schedules', 'SchedulesController@data');
                Route::get('/schedules/{uid}/{status}', 'SchedulesController@changeStatus')->name('schedules.change-status');
                Route::post('{report}/delivery', 'ReportsController@getDeliveryMethod');
            });

            Route::get('schedules/add/{report?}', 'SchedulesController@add')->name('schedules.add');
            Route::get('schedules/edit/{report?}', 'SchedulesController@edit')->name('schedules.edit');

            Route::resource('schedules', 'SchedulesController', ['only' => ['store']]);
        });

        Route::get('reports/{report}/{name?}', 'ReportsController@data')->name('reports.data');
        Route::post('reports/{report}/{name?}', 'ReportsController@data');
    });

    /**
     * Customers
     */
    Route::group(['prefix' => 'customers'], function () {
        Route::get('switch/{name}', 'CustomersController@switchCustomer')->name('customers.switch');
    });
});