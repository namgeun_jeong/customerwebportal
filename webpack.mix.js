let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copyDirectory('resources/assets/css/patterns', 'public/css/patterns');
mix.copyDirectory('resources/assets/img', 'public/img');
mix.copyDirectory('resources/assets/images', 'public/images');
mix.copyDirectory('resources/assets/fonts', 'public/fonts');
mix.copy('resources/assets/font-awesome/fonts', 'public/fonts');
mix.copy('resources/assets/img/chosen-sprite.png', 'public/css/chosen-sprite.png');
mix.copy('resources/assets/css/plugins/chosen/chosen-sprite@2x.png', 'public/css/chosen-sprite@2x.png');
mix.copy('resources/assets/css/plugins/iCheck/green.png', 'public/css/green.png');
mix.copy('resources/assets/css/plugins/iCheck/green@2x.png', 'public/css/green@2x.png');

mix.js([
    'resources/assets/js/app.js',
], 'public/js/app.js');

mix.scripts([
    'resources/assets/js/jquery-3.1.1.min.js',
    'resources/assets/js/bootstrap.min.js',

    'resources/assets/js/inspinia.js',
    'resources/assets/js/plugins/pace/pace.min.js',
    'resources/assets/js/plugins/slimscroll/jquery.slimscroll.min.js',

    'resources/assets/js/plugins/dataTables/datatables.min.js',
    'resources/assets/js/plugins/toastr/toastr.min.js',
    'resources/assets/js/plugins/datapicker/bootstrap-datepicker.js',
    'resources/assets/js/plugins/sweetalert/sweetalert.min.js',
    'resources/assets/js/plugins/chosen/chosen.jquery.js',
    'resources/assets/js/plugins/ladda/spin.min.js',
    'resources/assets/js/plugins/ladda/ladda.min.js',
    'resources/assets/js/plugins/ladda/ladda.jquery.min.js',

    'resources/assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js',
    'resources/assets/js/plugins/clockpicker/clockpicker.js',
    'resources/assets/js/plugins/iCheck/icheck.min.js',

    'resources/assets/js/plugins/metisMenu/jquery.metisMenu.js',
    'resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
], 'public/js/vendor.js');

mix.styles([
    'resources/assets/css/bootstrap.min.css',
    'resources/assets/font-awesome/css/font-awesome.min.css',

    'resources/assets/css/plugins/dataTables/datatables.min.css',
    'resources/assets/css/plugins/toastr/toastr.min.css',
    'resources/assets/css/plugins/datapicker/datepicker3.css',
    'resources/assets/css/plugins/sweetalert/sweetalert.css',
    'resources/assets/css/plugins/chosen/bootstrap-chosen.css',

    'resources/assets/css/plugins/ladda/ladda-themeless.min.css',
    'resources/assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',
    'resources/assets/css/plugins/iCheck/custom.css',

    'resources/assets/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css',
    'resources/assets/css/plugins/clockpicker/clockpicker.css',

    'resources/assets/css/animate.css',
    'resources/assets/css/style.css'
], 'public/css/vendor/vendors.css');

mix.styles([
    'public/css/vendor/vendors.css',
    'public/css/vendor/style.css',
    'public/css/vendor/responsive.css'
], 'public/css/vendor.css');

mix.styles('resources/assets/css/cwp.css', 'public/css/app.css');


// if (mix.inProduction()) {
//     mix.version();
// }