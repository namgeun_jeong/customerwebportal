<?php

return [

    'orders' => [
        'title' => 'Orders',
        'customer' => 'Customer Order',
        'work' => 'Work Order',
        'product-list' => 'Products List',
        'search' => 'Search',
    ],

    'requests' => [
        'title' => 'Customer Requests',
        'cancel' => [
            'title' => 'Request cancelled',
            'message' => 'Customer request was successfully cancelled'
        ],
        'update' => [
            'title' => 'Request updates',
            'message' => 'Customer request was successfully updated'
        ]
    ],




    'reports' => [
        'title' => 'Reports',
        'inventory' => 'Inventory',
        'postal-return' => 'Postal Return',
        'shipping-report' => 'Shipping report',

        'details' => [
            'title' => 'Details'
        ],


        'schedule' => [
            'title' => 'Schedule',
            'report' => 'Report',
            'starts' => 'Starts',
            'frequency' => [
                'title' => 'Repeat',
                'values' => [
                    'none' => 'None',
                    'every_day' => 'Every Day',
                    'every_week' => 'Every Week',
                    'every_fortnight' => 'Every Fortnight',
                    'every_month' => 'Every Month',
                    'every_year' => 'Every Year',
                ]
            ],
            'ends' => 'Ends',
            'delivery_method' => [
                'title' => 'Delivery Method',
                'values' => [
                    'email' => 'Email',
                    'sftp' => 'SFTP',
                ]
            ],
            'delivery_to' => 'Delivery to',
        ],
    ],

    'configurations' => [
        'title' => 'Configurations'
    ]

];
