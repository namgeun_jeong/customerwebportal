@extends('errors.layout')

@section('title', 'Forbidden')

@section('message', "Sorry, you don't have permission to do that.")
