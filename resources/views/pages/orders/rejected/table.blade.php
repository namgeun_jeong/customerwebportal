@component('partials.table', compact('data', 'pagination'))
    <table class="table table-striped table-bordered table-hover" id="dataTable">
        <thead>
        <tr>
            @foreach($data->columns as $column)
                <th @if ($column->maxWidth) style="width: {{ $column->maxWidth }}" @endif>
                    <div data-toggle="tooltip" data-placement="top" title="{{ $column->tooltips }}">
                        {{ $column->displayName }}
                    </div>
                </th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($data->data as $order)
            <tr>
                @foreach(get_object_vars($data->columns) as $key => $value)
                    <td>{{ $order->{$key} }}</td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
@endcomponent