@extends('layouts.cwp')

@section('title', __('Rejected Items'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('cwp.orders.title') }}
        </li>
        <li>
            <a href="{{ route('customers.data') }}">
                {{ __('cwp.orders.customer') }}
            </a>
        </li>
        <li class="active">
            <strong>{{ __('Rejected Items') }}</strong>
        </li>
    </ol>
@endsection

@section('content')
    <div id="table">
        @include('pages.orders.rejected.table')
    </div>
@endsection

@push('scripts')
    <script>
        var full_width = true;

        $(document).ready(function () {
            DTinit();
        });
    </script>
@endpush