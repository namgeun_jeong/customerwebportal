<div class="modal inmodal fade" id="order-items-details-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">{{ __('Order item details') }}</h2>
            </div>
            <div class="modal-body" id="order-item-details">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.order-items-details', function (e) {
                $.ajax({
                    method: "GET",
                    url: $(this).data('details-url'),
                    success: function (data) {
                        $('#order-item-details').html(data);
                        $('#order-items-details-modal').modal('show');
                    },
                    error: function (data) {
                        toastr.error(data.responseJSON.error, data.responseJSON.title);
                    }
                });
            });
        });
    </script>
@endpush