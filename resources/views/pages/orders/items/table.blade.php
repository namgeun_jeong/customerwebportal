@if (isset($data->searchFields) and !empty($data->searchFields))
    @include('partials.advanced-search')
@endif

@component('partials.table', compact('data', 'pagination'))
    <table class="table table-striped table-bordered table-hover" id="dataTable">
        <thead>
        <tr>
            <th style="width: 30px"></th>
            @if (isset($data->data) && !empty($data->columns))
                @foreach($data->columns as $column)
                    <th @if ($column->maxWidth) style="width: {{ $column->maxWidth }}" @endif>
                        <div data-toggle="tooltip" data-placement="top" title="{{ $column->tooltips }}">
                            {{ $column->displayName }}
                        </div>
                    </th>
                @endforeach
                <th style="width: 75px;"></th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($data->data as $item)
            <tr>
                <td class="text-center">
                    @if ($item->_status->tooltips == 'Shipped')
                        <div data-toggle="tooltip" data-placement="top" title="Shipped"
                             style="color: {{ $item->_status->color }}">
                            <span style="color: #192f5a;">
		 	                    <i class="fa fa-circle fa-2x"></i>
		                    <span>
                        </div>
                    @elseif($item->_status->tooltips == 'Destroyed')
                        <div data-toggle="tooltip" data-placement="top" title="{{ $item->_status->tooltips }}"
                             style="color: {{ $item->_status->color }}">
                            <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                        </div>
                    @elseif($item->_status->tooltips == 'In Progress')
                        <div data-toggle="tooltip" data-placement="top" title="{{ $item->_status->tooltips }}"
                             style="color: {{ $item->_status->color }}">
                            <span style="color: lightblue;">
		 	                    <i class="fa fa-adjust fa-2x"></i>
		                    <span>
                        </div>
                    @elseif($item->_status->tooltips == 'Pending')
                        <div data-toggle="tooltip" data-placement="top" title="{{ $item->_status->tooltips }}"
                             style="color: {{ $item->_status->color }}">
                            <span style="color: lightblue;">
                                <i class="fa fa-adjust fa-2x"></i>
		                    <span>
                        </div>
                    @elseif($item->_status->tooltips == 'Not Available')
                        <div data-toggle="tooltip" data-placement="top" title="Not Started"
                            style="color: {{ $item->_status->color }}">
                            <span style="color: lightblue;">
		 	                    <i class="fa fa-circle-thin fa-2x"></i>
		                    <span>
                        </div>
                    @elseif($item->_status->tooltips == 'Rejected')
                        <div data-toggle="tooltip" data-placement="top" title="{{ $item->_status->tooltips }}"
                             style="color: {{ $item->_status->color }}">
                            <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                        </div>
                    @elseif($item->_status->tooltips == 'Returned')
                        <div data-toggle="tooltip" data-placement="top" title="{{ $item->_status->tooltips }}"
                             style="color: {{ $item->_status->color }}">
                            <i class="fa fa-history fa-2x" aria-hidden="true"></i>
                        </div>
                    @else
                        <div data-toggle="tooltip" data-placement="top" title="Not Started"
                             style="color: {{ $item->_status->color }}">
                             <span style="color: lightblue;">
		 	                    <i class="fa fa-circle-thin fa-2x"></i>
		                    <span>
                        </div>
                    @endif
                </td>
                @foreach(get_object_vars($data->columns) as $key => $value)
                    <td>{{ $item->{$key} }}</td>
                @endforeach
                <td class="text-center">
                    <div style="width: 90px">
                        <button type="button" class="btn btn-outline btn-info order-items-details"
                                title="{{ __('Details') }}"
                                data-details-url="{{ route('order-items.show', ['order_item' => $item->_uid]) }}">
                            <i class="fa fa-info" aria-hidden="true"></i>
                        </button>

                        @if (!empty(json_decode($item->actions)))
                            <button type="button" class="btn btn-outline btn-primary pull-action"
                                    data-actions="{{ $item->actions }}" title="{{ __('Actions') }}"
                                    data-action-url="{{ route('order-items.actions', ['item_uid' => $item->_uid]) }}">
                                <i class="fa fa-cog" aria-hidden="true"></i>
                            </button>
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endcomponent