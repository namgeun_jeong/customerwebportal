@extends('layouts.cwp')

@section('title', __('Order Items'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('Orders') }}
        </li>
        <li>
            <a href="{{ route('work.data') }}" class="">{{ __('Work Order') }}</a>
        </li>
        <li class="active">
            <strong>{{ __('Order Items') }}</strong>
        </li>
    </ol>
@endsection

@section('content')

    <div id="table">
        @include('pages.orders.items.table')
    </div>

    @include('pages.orders.items.details.modal')

    @include('pages.orders.items.actions.modal')
@endsection

@push('scripts')
    <script>
        var offset_first = true;

        $(document).ready(function () {
            DTinit();
        });
    </script>
@endpush