<ul class="nav nav-tabs">
    @foreach($item_actions as $action)
        <li id="{{ $action }}" @if ($loop->first)class="active"@endif>
            <a data-toggle="tab" href="#tab-{{ $action }}">{{ $actions[$action]['tab'] }}</a>
        </li>
    @endforeach
</ul>

<div class="tab-content">
    @foreach($item_actions as $action)
        <div id="tab-{{ $action }}" class="tab-pane @if ($loop->first) active @endif">
            <div class="panel-body">
                <form class="form-horizontal action-form" method="post" action="{{ route('order-items.action') }}">
                    <input type="hidden" name="uid" value="{{ $item_uid }}">
                    <input type="hidden" name="action" value="{{ $action }}">

                    @include('pages.orders.items.actions.'. $action)

                    <div class="form-group">
                        <div class="col-sm-12">
                            <button class="btn btn-primary" data-style="zoom-in">{{ __('Submit') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach
</div>