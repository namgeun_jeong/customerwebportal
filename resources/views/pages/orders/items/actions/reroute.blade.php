<div class="form-group">
    <div class="col-sm-12">
        <input class="form-control" name="shippingDetail[companyName]" data-name="shippingDetail.companyName"
               placeholder="{{ __('Company Name') }}">
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <input class="form-control" name="shippingDetail[attentionTo]" data-name="shippingDetail.attentionTo"
               placeholder="{{ __('Attention To') }}">
    </div>
</div>

<div class="hr-line-dashed"></div>

<div class="form-group">
    <div class="col-sm-12">
        <input class="form-control" name="shippingDetail[address1]" data-name="shippingDetail.address1"
               placeholder="{{ __('Address 1') }}">
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <input class="form-control" name="shippingDetail[address2]" data-name="shippingDetail.address2"
               placeholder="{{ __('Address 2') }}">
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <input class="form-control" name="shippingDetail[address3]" data-name="shippingDetail.address3"
               placeholder="{{ __('Address 3') }}">
    </div>
</div>

<div class="hr-line-dashed"></div>

<div class="form-group">
    <div class="col-sm-12">
        <input class="form-control" name="shippingDetail[locality]" data-name="shippingDetail.locality"
               placeholder="{{ __('Locality') }}">
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <input class="form-control" name="shippingDetail[region]" data-name="shippingDetail.region"
               placeholder="{{ __('Region') }}">
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="col-sm-12">
                <input class="form-control" name="shippingDetail[postcode]" data-name="shippingDetail.postcode"
                       placeholder="{{ __('Zip Code') }}">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="col-sm-12">
                <select data-placeholder="Choose a Country..." class="form-control"
                        name="shippingDetail[countryCode]" data-name="shippingDetail.countryCode">
                    <option value="">{{ __('Select country') }}</option>
                    @foreach($countries as $code => $name)
                        <option value="{{ $code }}">{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

<div class="hr-line-dashed"></div>

<div class="form-group">
    <div class="col-sm-12">
        <input class="form-control" name="shippingDetail[email]"
               data-name="shippingDetail.email"
               placeholder="{{ __('Email') }}">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        <input class="form-control" name="shippingDetail[phone]"
               data-name="shippingDetail.phone"
               placeholder="{{ __('Phone') }}">
    </div>
</div>

<div class="hr-line-dashed"></div>

<div class="form-group" id="delivery_date" style="display: none;">
    <label class="col-sm-3 control-label">{{ __('Delivery date') }}</label>
    <div class="col-sm-9">
        <div class="input-group date" data-name="delivery_date">
            <span class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </span>
            <input class="form-control" name="delivery_date" value="">
        </div>
        <span class="help-block m-b-none">Optional.</span>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <div class="i-checks">
            <label>
                <input type="checkbox" value="true" name="prioritize" id="prioritize">
                <i></i> {{ __('Prioritize') }}
            </label>
        </div>
    </div>
</div>

