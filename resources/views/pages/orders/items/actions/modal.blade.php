<div class="modal inmodal fade" id="actions-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h2 class="modal-title">{{ __('Pull Actions') }}</h2>
            </div>
            <div class="modal-body">
                <div class="tabs-container">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {

            // Call actions modal
            $(document).on('click', '.pull-action', function (e) {
                var actions = $(this).data('actions');

                $.ajax({
                    type: 'POST',
                    url: $(this).data('action-url'),
                    data: {actions: actions},
                    dataType: 'html',
                    success: function (data) {
                        $('#actions-modal .tabs-container').html(data);

                        $('#delivery_date .input-group.date').datepicker({
                            keyboardNavigation: false,
                            format: "yyyy-mm-dd",
                            calendarWeeks: true,
                            autoclose: true
                        });

                        $('.i-checks').iCheck({
                            checkboxClass: 'icheckbox_square-green',
                            radioClass: 'iradio_square-green',
                        });

                        $('#actions-modal').modal('show');
                    },
                    error: function (data) {
                        alert('Something goes wrong!');
                    }
                });
            });

            // Send a pull action
            $('#actions-modal').on('submit', '.action-form', function (e) {
                e.preventDefault();

                var l = Ladda.create($(this).find('button')[0]);
                l.start();

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: $(this).serializeArray(),
                    dataType: 'json',
                    success: function (data) {
                        if (data.error !== undefined) {
                            toastr.error(data.error, data.title);
                        } else {
                            $('#actions-modal').modal('hide');
                            toastr.success(data.message, data.title);
                            getData();
                        }
                    },
                    error: function (data) {
                        var errors = data.responseJSON.errors;
                        $('.error-note').remove();
                        $('.form-group').removeClass('has-error');
                        $.each(errors, function (key, value) {
                            var field = $('form').find('[data-name="' + key + '"]');
                            var form_group = field.closest('.form-group');
                            form_group.addClass('has-error');
                            field.after('<span class="help-block m-b-none error-note">' + value + '</span>');
                        });
                    }
                }).always(function () {
                    l.stop();
                });
            });

            $(document).on('ifUnchecked', '#prioritize', function (event) {
                $('#delivery_date').hide();
            });

            $(document).on('ifChecked', '#prioritize', function (event) {
                $('#delivery_date').show();
            });

            // Clear actions modal
            $('.modal').on('hidden.bs.modal', function (e) {
                $('#actions-modal .tabs-container').html('');
            });
        });
    </script>
@endpush