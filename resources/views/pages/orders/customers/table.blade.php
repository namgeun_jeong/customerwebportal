@component('partials.table', compact('data', 'pagination'))
    <table class="table table-striped table-bordered table-hover" id="dataTable">
        <thead>
        <tr>
            @foreach($data->columns as $column)
                <th @if ($column->maxWidth) style="width: {{ $column->maxWidth }}" @endif>
                    <div data-toggle="tooltip" data-placement="top" title="{{ $column->tooltips }}">
                        {{ $column->displayName }}
                    </div>
                </th>
            @endforeach
            <th style="width: 82px;"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data->data as $order)
            <tr>
                @foreach(get_object_vars($data->columns) as $key => $value)
                    @if ($key == 'date')
                        <td>{{ Carbon\Carbon::parse($order->{$key})->toDateString() }}</td>
                    @elseif ($key == 'rejectedQuantity')
                        @if ($order->{'#hasRejectedItems'})
                            <td>
                                <a target="_blank"
                                   href="{{ route('rejected.data', ['customer_order_uid' => $order->_uid ]) }}"
                                   class="text-cwp" title="{{ __('Rejected Items') }}">
                                    {{ number_format( $order->{$key} , 0 , '.' , ',' ) }}
                                </a>
                            </td>
                        @else
                            <td>{{ $order->{$key} }}</td>
                        @endif
                    @elseif (in_array($key, ['totalQuantity', 'rejectedQuantity', 'productionQuantity', 'destroyedQuantity', 'reroutedQuantity', 'prioritizedQuantity']))
                        <td>{{ number_format( $order->{$key} , 0 , '.' , ',' ) }}</td>
                    @else
                        <td>{{ $order->{$key} }}</td>
                    @endif
                @endforeach
                <td class="text-center">
                    <div style="width: 90px">
                        @if ($order->{'#workOrder'})
                            <a href="{{ route('work.customer', ['customer_order_uid' => $order->_uid]) }}"
                               class="btn btn-outline btn-success work-order" title="{{ __("Work Orders") }}">
                                <i class="fa fa-list-alt" aria-hidden="true"></i>
                            </a>
                        @endif

                        @if ($order->{'#cancel'})
                            <a href="#" class="btn btn-outline btn-warning" title="{{ __("Cancel") }}">
                                <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                            </a>
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endcomponent