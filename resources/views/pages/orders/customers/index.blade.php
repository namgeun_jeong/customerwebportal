@extends('layouts.cwp')

@section('title', __('Customer Orders'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('cwp.orders.title') }}
        </li>
        <li class="active">
            <strong>{{ __('Customer Orders') }}</strong>
        </li>
    </ol>
@endsection

@section('content')
    <div class="ibox-content m-b-md border-bottom p-top-xs-20 p-top-sm-20">
        <div class="row">
            <div class="col-md-5 col-lg-3">
                @include('partials.date-range')
            </div>
        </div>
    </div>

    <div id="table">
        @include('pages.orders.customers.table')
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            DTinit();

            $(document).on('click', '.work-order', function (e) {
                var date_from = $("#date_from").val();
                var date_to = $("#date_to").val();
                location.href = $(this).data('action-url') + '?date_from=' + date_from + '&date_to=' + date_to;
            });
        });
    </script>
@endpush