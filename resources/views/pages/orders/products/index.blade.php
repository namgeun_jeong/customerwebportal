@extends('layouts.cwp')

@section('title', __('Product Items'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('Orders') }}
        </li>
        <li class="active">
            <strong>{{ __('Product Items') }}</strong>
        </li>
    </ol>
@endsection

@section('content')

    @if (isset($verticals))
        <div class="ibox-content m-b-md border-bottom p-top-xs-20 p-top-sm-20">

            <product-items verticals-list="{{ json_encode($verticals) }}"
                           current-vertical="{{ $selectedVertical }}"></product-items>

        </div>
    @endif

    @if (isset($data))
        <div id="table">
            @include('pages.orders.products.table')
        </div>

        @include('pages.orders.items.details.modal')

        @include('pages.orders.items.actions.modal')
    @else
        <div class="alert alert-warning m-n">
            {{ __('No data available.') }}
        </div>
    @endif

@endsection

@push('scripts')
    <script>
        var offset_first = true;

        $(document).ready(function () {
            DTinit();

            $('.chosen-select.verticals').change(function () {
                $('.chosen-select.products').val('');
                getData();
            });
        });
    </script>
@endpush