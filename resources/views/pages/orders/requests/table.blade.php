@component('partials.table', compact('data', 'pagination'))
    <table class="table table-striped table-bordered table-hover" id="dataTable">
        <thead>
        <tr>
            @foreach($data->columns as $column)
                <th @if ($column->maxWidth) style="width: {{ $column->maxWidth }}" @endif>
                    <div data-toggle="tooltip" data-placement="top" title="{{ $column->tooltips }}">
                        {{ $column->displayName }}
                    </div>
                </th>
            @endforeach
            <th style="width: 170px"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data->data as $item)
            <tr>
                @foreach(get_object_vars($data->columns) as $key => $value)
                    @if ($key == 'requestedOn' || $key == 'lastUpdated')
                        <td>{{ Carbon\Carbon::parse($item->{$key})->toDateTimeString() }}</td>
                    @elseif ($key == 'status')
                        @php
                            $class = '';
                            switch ($item->status) {
                                case 'Rejected': $class = 'text-danger'; break;
                                case 'Cancelled': $class = 'text-warning'; break;
                                case 'Closed': $class = 'text-success'; break;
                                case 'InProgress': $class = 'text-info'; break;
                                case 'Pending': $class = 'text-primary'; break;
                            }
                        @endphp
                        <td><span class="{{ $class }}">{{ $item->{$key} }}</span></td>
                    @elseif ($key == 'refNo')
                        <td>
                            <button data-details-url="{{ route('order-items.show', $item->_orderItemuid) }}"
                                    class="btn btn-link text-cwp order-details" title="{{ __('Order details') }}">
                                {{ $item->{$key} }}
                            </button>
                        </td>
                    @else
                        <td>{{ $item->{$key} }}</td>
                    @endif
                @endforeach
                <td class="text-center">
                    <div style="width: 120px; margin: 0 auto;">
                        @if ($item->{'#hasDetails'})
                            <a target="_blank" href="{{ route('requests.show', $item->_uid) }}"
                               class="btn btn-info btn-outline" title="{{ __("Details") }}">
                                <i class="fa fa-info" aria-hidden="true"></i>
                            </a>
                        @endif

                        @if (\App\User::roleIn(['RequestManager', 'ABCorp Account Manager', 'ABCorp Admin', 'OrderManager', 'OrderUser']))
                            @if ($item->{'#canUpdate'})
                                <button href="#" class="btn btn-primary btn-outline request-update"
                                        data-uid="{{ $item->_uid }}" title="{{ __("Update") }}">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </button>
                            @endif
                        @endif

                        @if ($item->{'#canCancel'})
                            <a href="{{ route('requests.cancel', $item->_uid) }}"
                               class="btn btn-outline btn-warning cancel-request alert-warning" title="{{ __("Cancel") }}">
                                <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                            </a>
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endcomponent