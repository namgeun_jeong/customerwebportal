<form class="form-horizontal">
    @if ($data->details->remark)
        <div class="form-group">
            <label class="col-lg-4 control-label">{{ __("Comment") }}:</label>
            <div class="col-lg-8">
                <p class="form-control-static">{{ $data->details->remark }}</p>
            </div>
        </div>
    @elseif (count(json_decode($data->details->requestDetails)->shippingDetail))
        @foreach(json_decode($data->details->requestDetails)->shippingDetail as $key => $item)
            @if ($item)
                <div class="form-group">
                    <label class="col-lg-4 control-label">{{ $key }}:</label>
                    <div class="col-lg-8">
                        <p class="form-control-static">
                            @if($key == 'countryCode')
                                {{ isset($countries[$item]) ? $countries[$item] : $item}}
                            @else
                                {{ $item }}
                            @endif
                        </p>
                    </div>
                </div>
            @endif
        @endforeach
    @else
        <div class="alert alert-warning m-n">
            {{ __('No data available.') }}
        </div>
    @endif
</form>