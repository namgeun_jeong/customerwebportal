@extends('layouts.cwp')

@section('title', __('cwp.requests.title'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('cwp.orders.title') }}
        </li>
        <li class="active">
            <strong>{{ __('cwp.requests.title') }}</strong>
        </li>
    </ol>
@endsection

@section('content')
    <div class="ibox-content m-b-md border-bottom p-top-xs-20 p-top-sm-20">
        <div class="row">
            <div class="col-md-4 m-bottom-xs-10 m-bottom-sm-10 m-bottom-md-0">
                @include('partials.date-range')
            </div>

            <div class="col-md-4 m-bottom-xs-10 m-bottom-sm-10 m-bottom-md-0">
                <select data-placeholder="Select actions" class="chosen-select actions" multiple>
                    @foreach($data->actionList as $key => $action)
                        <option value="{{ $action }}">{{ $action }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-4">
                <select data-placeholder="Select statuses" class="chosen-select statuses" multiple>
                    @foreach($data->statusList as $status)
                        <option value="{{ $status }}">{{ $status }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div id="table">
        @include('pages.orders.requests.table')
    </div>

    <div class="modal inmodal fade" id="order-details-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h2 class="modal-title">{{ __('Order details') }}</h2>
                </div>
                <div class="modal-body">
                    <div id="order-details"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="update-request" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">{{ __('Close') }}</span>
                    </button>
                    <h4 class="modal-title">{{ __('Request update') }}</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="button" class="btn btn-primary submit"
                            data-style="zoom-in">{{ __('Submit') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {

            DTinit();

            $('.chosen-select').chosen({width: "100%"}).change(function () {
                getData();
            });

            $(document).on('click', '.order-details', function (e) {
                e.preventDefault();
                $.ajax({
                    method: "GET",
                    url: $(this).data('details-url'),
                    success: function (data) {
                        $('#order-details').html(data);
                        $('#order-details-modal').modal('show');
                    },
                    error: function (data) {
                        toastr.error(data.responseJSON.error, data.responseJSON.title);
                    }
                });
            });

            $(document).on('click', '.cancel-request', function (e) {
                e.preventDefault();

                var href = $(this).attr('href');

                swal({
                    title: "Are you sure?",
                    text: "Do you want to cancel the request?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, cancel it!",
                    closeOnConfirm: false
                }, function () {
                    $.ajax({
                        method: "POST",
                        url: href,
                        success: function (data) {
                            if (data.error) {
                                toastr.error(data.error, data.title);
                            } else {
                                getData();
                                toastr.success(data.message, data.title);
                                swal("Canceled!", "Customer request has been canceled.", "success");
                            }
                        },
                        error: function (data) {
                        }
                    });
                });
            });

            $(document).on('click', '.request-update', function (e) {
                $.ajax({
                    method: "GET",
                    url: "{{ url('orders/requests') }}/" + $(this).data('uid') + "/edit"
                }).done(function (data) {
                    $('#update-request .modal-body').html(data);
                    $("#update-request").modal('show');
                });
            });

            $('#update-request').on('click', '.submit', function (e) {
                var modal = $(this).closest('.modal');
                var form = modal.find('form');

                var l = Ladda.create(this);
                l.start();

                $.ajax({
                    type: 'PUT',
                    url: form.attr('action'),
                    data: form.serializeArray(),
                    dataType: 'json',
                    success: function (data) {
                        if (data.error) {
                            toastr.error(data.error, data.title);
                        } else {
                            modal.modal('hide');
                            toastr.success(data.message, data.title);
                        }
                        getData();
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        $('#form-update-request .form-group').removeClass('has-error');
                        $.each(errors, function (key, value) {
                            var field = $('#form-update-request').find('#' + key);
                            var form_group = field.closest('.form-group');
                            form_group.addClass('has-error');
                            field.after('<span class="help-block m-b-none error-note">' + value + '</span>');
                        });
                    }
                }).always(function() { l.stop(); });
            });
        });
    </script>
@endpush