<form method="PUT" class="form-horizontal" id="form-update-request"
      action="{{ route('requests.update', $data->details->_uid) }}">

    <div class="form-group">
        <label class="col-sm-3 control-label" for="message">{{ __('Message') }}</label>
        <div class="col-sm-9">
            <input name="message" id="message" placeholder="" class="form-control m-b">
        </div>
    </div>

    <div class="hr-line-dashed"></div>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="status">{{ __('Status') }}</label>
        <div class="col-sm-9">
            <select class="form-control m-b" name="status" id="status">
                @foreach($statuses as $status)
                    <option value="{{ $status }}" @if ($data->details->status == $status) selected @endif>
                        {{ $status }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    @if (in_array($data->details->requestedAction, ['PullAndForward', 'PullAndRush']))

        <div class="hr-line-dashed"></div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="carrier">{{ __('Carrier') }}</label>
            <div class="col-sm-9">
                <select class="form-control m-b" name="carrier" id="carrier">
                    <option value="">Select carrier</option>
                    @foreach($carriers as $carrier)
                        <option value="{{ $carrier }}">{{ $carrier }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="hr-line-dashed"></div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="trackingNo">{{ __('Tracking number') }}</label>
            <div class="col-sm-9">
                <input name="trackingNo" id="trackingNo" placeholder="" class="form-control m-b">
            </div>
        </div>
    @endif
</form>
