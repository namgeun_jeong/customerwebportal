@extends('layouts.cwp')

@section('title', __('Customer Request Details'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('cwp.orders.title') }}
        </li>
        <li>
            <a href="{{ route('requests.data') }}">
                {{ __('cwp.requests.title') }}
            </a>
        </li>
        <li class="active">
            <strong>{{ __('Details') }}</strong>
        </li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-3">

            <div class="ibox">
                <div class="ibox-title">
                    <h2>{{ __('Details') }}</h2>
                </div>
                <div class="ibox-content m-b-sm border-bottom">
                    <form class="form-horizontal">
                        @if ($data->details->remark)
                            <div class="form-group">
                                <label class="col-lg-4 control-label">{{ __("Comment") }}:</label>
                                <div class="col-lg-8">
                                    <p class="form-control-static">{{ $data->details->remark }}</p>
                                </div>
                            </div>
                        @endif

                        @if (count($shippingDetail))
                            @foreach($shippingDetail as $key => $item)
                                @if ($item)
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">{{ $key }}:</label>
                                        <div class="col-lg-8">
                                            <p class="form-control-static">
                                                @if($key == 'countryCode')
                                                    {{ isset($countries[$item]) ? $countries[$item] : $item}}
                                                @else
                                                    {{ $item }}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                            @if ($rushForwardRequested)
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Prioritize:</label>
                                    <div class="col-lg-8">
                                        <p class="form-control-static">
                                            {{ (bool)$rushForwardRequested }}
                                        </p>
                                    </div>
                                </div>
                            @endif
                        @endif
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-9">
            <div class="ibox">
                <div class="ibox-title">
                    <h2>{{ __('Response history') }}</h2>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTable">
                            <thead>
                            <tr>
                                @foreach($data->columns as $column)
                                    <th>{{ $column->displayName }}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($data->data))
                                @foreach($data->data as $item)
                                    <tr>
                                        @foreach(get_object_vars($data->columns) as $key => $value)

                                            @if ($key == 'date')
                                                <td>{{ Carbon\Carbon::parse($item->{$key})->toDateTimeString() }}</td>
                                            @elseif ($key == 'status')
                                                @php
                                                    $class = '';
                                                    switch ($item->status) {
                                                        case 'Rejected': $class = 'text-danger'; break;
                                                        case 'Cancelled': $class = 'text-warning'; break;
                                                        case 'Closed': $class = 'text-success'; break;
                                                        case 'InProgress': $class = 'text-info'; break;
                                                        case 'Pending': $class = 'text-primary'; break;
                                                    }
                                                @endphp
                                                <td><span class="{{ $class }}">{{ $item->{$key} }}</span></td>
                                            @else
                                                <td>{{ $item->{$key} }}</td>
                                            @endif

                                        @endforeach
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {

            $('#dataTable').DataTable({
                "order": [[0, "desc"]],
                "dom": 't',
                "autoWidth": false,
                drawCallback: function (settings) {
                    var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
                    pagination.toggle(this.api().page.info().pages > 1);
                }
            });
        });
    </script>
@endpush