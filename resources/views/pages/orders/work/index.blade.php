@extends('layouts.cwp')

@section('title', __('Work Orders'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('cwp.orders.title') }}
        </li>
        <li class="active">
            <strong>{{ __('Work Orders') }}</strong>
        </li>
    </ol>
@endsection

@section('content')

    <div class="ibox-content m-b-md border-bottom p-top-xs-20 p-top-sm-20">
        <div class="row">
            @if ($customer_order_uid)
            @else
            <div class="col-md-5 col-sm-12 col-xs-12 m-bottom-xs-10 m-bottom-sm-10 m-bottom-md-0">
                @include('partials.date-range')
            </div>
            @endif

            <div class="col-md-3 col-sm-6 col-xs-12 m-bottom-xs-10 m-bottom-sm-0 pull-right">
                <select data-placeholder="Select products" class="chosen-select products" multiple>
                    @foreach($data->products as $product)
                        <option value="{{ $product->key }}">{{ $product->value }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12 pull-right">
                <select data-placeholder="Select statuses" class="chosen-select statuses" multiple>
                    @foreach($data->statuses as $status)
                        @if ($status == 'Not Available')
                            <option value="{{ $status }}">{{ 'Not Started' }}</option>
                        @else
                            <option value="{{ $status }}">{{ $status }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div id="table">
        @include('pages.orders.work.table')
    </div>

    <div class="modal inmodal fade" id="order-details-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h2 class="modal-title">{{ __('Work order details') }}</h2>
                </div>
                <div class="modal-body">
                    <div id="order-details"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var offset_first = true;

        $(document).ready(function () {
            DTinit();

            $('.chosen-select').chosen({width: "100%"}).change(function () {
                getData();
            });
        });

        $(document).on('click', '.order-details', function (e) {
            e.preventDefault();
            $.ajax({
                method: "GET",
                url: $(this).data('details-url'),
                success: function (data) {
                    $('#order-details').html(data);
                    $('#order-details-modal').modal('show');
                },
                error: function (data) {
                    toastr.error(data.responseJSON.error, data.responseJSON.title);
                }
            });
        });
    </script>
@endpush