@component('partials.table', compact('data', 'pagination'))
    <table class="table table-striped table-bordered table-hover" id="dataTable">
        <thead>
        <tr>
            <th style="width: 30px"></th>
            @foreach($data->columns as $column)
                <th @if ($column->maxWidth) style="width: {{ $column->maxWidth }}" @endif>
                    <div data-toggle="tooltip" data-placement="top" title="{{ $column->tooltips }}">
                        {{ $column->displayName }}
                    </div>
                </th>
            @endforeach
            <th style="width: 40px"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data->data as $order)
            <tr>
                <td class="text-center">
                    @if ($order->status == 'Shipped')
                        <div data-toggle="tooltip" data-placement="top" title="Shipped"
                             style="color: {{ $order->_status->color }}">
                            <span style="color: #192f5a;">
		 	                    <i class="fa fa-circle fa-2x"></i>
		                    <span>
                        </div>
                    @elseif($order->status == 'Destroyed')
                        <div data-toggle="tooltip" data-placement="top" title="{{ $order->_status->tooltips }}"
                             style="color: {{ $order->_status->color }}">
                            <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                        </div>
                    @elseif($order->status == 'In Progress')
                        <div data-toggle="tooltip" data-placement="top" title="{{ $order->_status->tooltips }}"
                             style="color: {{ $order->_status->color }}">
                            <span style="color: lightblue;">
		 	                    <i class="fa fa-adjust fa-2x"></i>
		                    <span>
                        </div>
                    @elseif($order->status == 'Pending')
                        <div data-toggle="tooltip" data-placement="top" title="{{ $order->_status->tooltips }}"
                             style="color: {{ $order->_status->color }}">
                             <span style="color: lightblue;">
		 	                    <i class="fa fa-adjust fa-2x"></i>
		                    <span>
                        </div>
                    @elseif($order->status == 'Not Available ')
                        <div data-toggle="tooltip" data-placement="top" title="{{ $order->_status->tooltips }}"
                            style="color: {{ $order->_status->color }}">
                            <span style="color: lightblue;">
		 	                    <i class="fa fa-circle-thin fa-2x"></i>
		                    <span>
                        </div>
                    @elseif($order->status == 'Rejected')
                        <div data-toggle="tooltip" data-placement="top" title="{{ $order->_status->tooltips }}"
                             style="color: {{ $order->_status->color }}">
                            <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                        </div>
                    @elseif($order->status == 'Returned')
                        <div data-toggle="tooltip" data-placement="top" title="{{ $order->_status->tooltips }}"
                             style="color: {{ $order->_status->color }}">
                            <i class="fa fa-history fa-2x" aria-hidden="true"></i>
                        </div>
                    @else
                        <div data-toggle="tooltip" data-placement="top" title="Not Started"
                             style="color: {{ $order->_status->color }}">
                            <span style="color: lightblue;">
		 	                    <i class="fa fa-circle-thin fa-2x"></i>
		                    <span>
                        </div>
                    @endif
                </td>
                @foreach(get_object_vars($data->columns) as $key => $value)
                    @if ($key == 'date')
                        <td>{{ Carbon\Carbon::parse($order->{$key})->toDateString() }}</td>
                    @elseif($key == 'workOrderID')
                        <td>
                            <a href="#" data-details-url="{{ route('work.show', ['work_order_uid' => $order->_uid]) }}"
                                    class="btn btn-outline btn-info order-details" title="{{ __('Order details') }}">
                                {{ $order->{$key} }}
                            </a>
                        </td>
                    @elseif (in_array($key, ['totalQuantity', 'rejectedQuantity', 'productionQuantity', 'destroyedQuantity', 'reroutedQuantity', 'prioritizedQuantity']))
                        <td>{{ number_format( $order->{$key} , 0 , '.' , ',' ) }}</td>
                    @else
                        <td>{{ $order->{$key} }}</td>
                    @endif
                @endforeach
                <td class="text-center">
                    @if ($order->{'#productList'})
                        <a href="{{ route('order-items.order', ['work_order_uid' => $order->_uid]) }}"
                           class="btn btn-outline btn-success" title="{{ __('Product List') }}">
                            <i class="fa fa-list-alt" aria-hidden="true"></i>
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endcomponent