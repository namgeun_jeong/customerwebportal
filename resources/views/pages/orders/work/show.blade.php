<form class="form-horizontal">
    @foreach($details->columns as $key => $column)
        <div class="form-group">
            <label class="col-sm-4 control-label">{{ $column->displayName }}</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{ $details->data[0]->{$key} }}</p>
            </div>
        </div>
    @endforeach
</form>