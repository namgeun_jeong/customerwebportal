@extends('layouts.cwp')

@section('title', __('Product Management'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('Administration') }}
        </li>
        <li class="active">
            <strong>{{ __('Product Management') }}</strong>
        </li>
    </ol>
@endsection

@section('content')
    <visual-product-management></visual-product-management>
@endsection