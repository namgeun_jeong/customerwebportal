@extends('layouts.cwp')

@section('title', __('Add new user'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('Administration') }}
        </li>
        <li>
            <a href="{{ route('users.index') }}">
                {{ __('Users') }}
            </a>
        </li>
        <li class="active">
            <strong>{{ __('Add new user') }}</strong>
        </li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="{{ route('users.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            <label class="col-sm-2 control-label" for="email">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="email" id="email"
                                       value="{{ old('email') }}">
                                @if ($errors->has('email')) <p
                                        class="help-block">{{ $errors->first('email') }}</p> @endif
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('vendor-css')
    <link href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
@endpush

@push('vendor-scripts')
    <script src="{{ asset('js/plugins/chosen/chosen.jquery.js') }}"></script>
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.chosen-select').chosen();
        });
    </script>
@endpush