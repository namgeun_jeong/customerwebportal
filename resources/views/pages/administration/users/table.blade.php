<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content table-data">
                @if (isset($members))
                    <div class="table-responsive">

                        <table class="table table-striped table-bordered table-hover" id="dataTable">
                            <thead>
                            <tr>
                                <th style="width: 50px;"></th>
                                <th>{{ __('Email') }}</th>
                                <th style="width: 100px;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($members as $member)
                                <tr>
                                    <td>
                                        <img class="img-responsive img-circle"
                                             src="{{ isset($member->user_metadata->picture) ?
                                             $member->user_metadata->picture : $member->picture }}">
                                    </td>
                                    <td>
                                        {{ $member->email }}
                                    </td>
                                    <td class="text-center">
                                        <div style="width: 90px">
                                            <a href="{{ route('users.show', $member->user_id) }}"
                                               class="btn btn-outline btn-info" title="{{ __("User info") }}">
                                                <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                            </a>

                                            <form action="{{ route('users.destroy', $member->user_id) }}" method="post"
                                                  style="display: inline">
                                                {{ method_field('DELETE') }}
                                                {{csrf_field()}}

                                                <button title="{{ __('Delete') }}" class="btn btn-outline btn-danger delete"
                                                        type="submit">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-warning m-n">
                        {{ __('No data available.') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>