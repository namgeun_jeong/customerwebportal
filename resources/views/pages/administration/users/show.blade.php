@extends('layouts.cwp')

@section('title', __('Users info'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('Administration') }}
        </li>
        <li>
            <a href="{{ route('users.index') }}">
                {{ __('Users') }}
            </a>
        </li>
        <li class="active">
            <strong>{{ $user->nickname ?? 'No nickname' }}</strong>
        </li>
    </ol>
@endsection

@section('content')
    <div class="row m-b-lg m-t-lg">
        <div class="col-md-5">

            <div class="profile-image">
                <img src="{{ isset($user->user_metadata->picture) ? $user->user_metadata->picture : $user->picture }}"
                     class="img-circle circle-border m-b-md" alt="profile">
            </div>
            <div class="profile-info">
                <div class="">
                    <div>
                        <h2>
                            {{ $user->nickname ?? 'No nickname' }}
                        </h2>

                        <p>
                            <button class="btn btn-primary" id="password_reset"
                                    data-url="{{ route('users.password-reset', $user->user_id) }}">
                                <i class="fa fa-envelope"></i>
                                Send password reset link
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <table class="table small m-b-xs">
                <tbody>
                <tr>
                    <td>ID: <strong>{{ $user->user_id }}</strong></td>
                    <td>Email Verified: <strong>{{ $user->email_verified ? 'Yes' : 'No' }}</strong></td>
                </tr>
                <tr>
                    <td>Email: <strong>{{ $user->email }}</strong></td>
                    <td>
                        Created at: <strong>{{ Carbon\Carbon::parse($user->created_at)->toDateTimeString() }}</strong>
                    </td>
                </tr>
                <tr>
                    <td>Name: <strong>{{ $user->name ?? 'No name' }}</strong></td>
                    <td>
                        Updated at: <strong>{{ Carbon\Carbon::parse($user->updated_at)->toDateTimeString() }}</strong>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-3">
            <small>Logins count</small>
            <h2 class="no-margins">{{ $user->logins_count ?? 0 }}</h2>

            @if (isset($user->last_login))
                <small>
                    Last login:
                    <strong>{{ Carbon\Carbon::parse($user->last_login)->toDateTimeString() }}</strong>
                </small>
            @endif
            <div id="sparkline1"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>User roles</h5>
                </div>
                <div class="ibox-content">
                    <form action="{{ route('users.update-roles', $user->user_id) }}" method="post" id="update-role">
                        <div class="form-group">
                            <select name="roles[]" data-placeholder="Choose roles" class="chosen-select" multiple
                                    style="width:350px;" id="roles" tabindex="4">
                                @foreach($roles as $key => $value)
                                    @if (in_array($value, $user_roles))
                                        <option value="{{ $key }}" selected> {{ $value }}</option>
                                    @else
                                        <option value="{{ $key }}"> {{ $value }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <button type="button" class="btn btn-primary btn-sm btn-block" data-style="zoom-in">Update
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {

            $('.chosen-select').chosen();

            $('#password_reset').on('click', function (e) {
                let l = Ladda.create(this);
                l.start();

                $.ajax({
                    type: 'POST',
                    url: $(this).data('url'),
                    success: function (data) {
                        if (data.error) {
                            toastr.error(data.error, data.title);
                        } else {
                            toastr.success(data.message, data.title);
                        }
                    }
                }).always(function () {
                    l.stop();
                });
            });

            $('#update-role').on('click', 'button', function (e) {

                let l = Ladda.create(this);
                l.start();

                $.ajax({
                    type: 'POST',
                    url: $('#update-role').attr('action'),
                    data: $('#update-role').serialize(),
                    success: function (data) {
                        if (data.error) {
                            toastr.error(data.error);
                        } else {
                            toastr.success(data.message);
                        }
                    }
                }).always(function () {
                    l.stop();
                });
            });
        });
    </script>
@endpush