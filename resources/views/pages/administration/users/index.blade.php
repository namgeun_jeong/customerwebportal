@extends('layouts.cwp')

@section('title', __('Users'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('Administration') }}
        </li>
        <li class="active">
            <strong>{{ __('Users') }}</strong>
        </li>
    </ol>
@endsection

@section('content')
    <div class="ibox-content m-b-md border-bottom p-top-xs-20 p-top-sm-20">
        <div class="row">
            <div class="col-md-2 col-xs-12 pull-right">
                <a href="{{ route('users.create') }}" class="btn btn-success btn-outline btn-xs-block pull-right">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    {{ __('Add new user') }}
                </a>
            </div>
        </div>
    </div>

    <div id="table">
        @include('pages.administration.users.table')
    </div>
@endsection

@push('scripts')
    <script>
        let offset_first = true;

        $(document).ready(function () {
            let columnDefs = initDTSearchFields();

            let table = $('#dataTable').DataTable({
                "dom": 'tp',
                "autoWidth": false,
                drawCallback: function (settings) {
                    let pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
                    pagination.toggle(this.api().page.info().pages > 1);
                },
                "columnDefs": columnDefs
            });

            DTIndividualSearch(table);
        });
    </script>
@endpush