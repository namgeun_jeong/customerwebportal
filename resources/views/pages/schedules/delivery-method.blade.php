@if (isset($schedule->deliveryMethod) && $schedule->deliveryMethod == 0)
    <div class="form-group" id="to_emails">
        <label class="col-sm-3 control-label">{{ __('Emails') }}</label>
        <div class="col-sm-9">
            <input class="form-control" type="text" name="emails" id="emails"
                   value="{{$schedule->deliveryData }}"/>
            <span class="help-block m-b-none">
                You can add multiple emails divided by ';'
            </span>
        </div>
    </div>
@else
    <div class="form-group" id="to_emails" @if(!isset($report) || !$report || (isset($schedule->deliveryMethod) && $schedule->deliveryMethod == 1)) style="display: none" @endif>
        <label class="col-sm-3 control-label">{{ __('Emails') }}</label>
        <div class="col-sm-9">
            <input class="form-control" type="text" name="emails" id="emails">
            <span class="help-block m-b-none">
                You can add multiple emails divided by ';'
            </span>
        </div>
    </div>
@endif

@if (count($endpoints))
    @if (isset($schedule->deliveryMethod) && $schedule->deliveryMethod == 1)
        <div class="form-group" id="to_endpoint">
            <label class="col-sm-3 control-label">{{ __('Endpoint') }}</label>
            <div class="col-sm-9">
                <select class="form-control" name="endpoint" id="endpoint">
                    @foreach($endpoints as $endpoint)
                        <option value="{{ $endpoint->_uid }}"
                                {{$schedule->deliveryData == $endpoint->_uid ? 'selected' : ''}}>{{ $endpoint->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    @else
        <div class="form-group" id="to_endpoint" style="display: none">
            <label class="col-sm-3 control-label">{{ __('Endpoint') }}</label>
            <div class="col-sm-9">
                <select class="form-control" name="endpoint" id="endpoint">
                    @foreach($endpoints as $endpoint)
                        <option value="{{ $endpoint->_uid }}">{{ $endpoint->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    @endif
@endif