<form class="form-horizontal text-left" id="schedule-form">

    @if (isset($schedule))
        <input type="hidden" name="schedule_uid" value="{{ $schedule->_uid }}">
    @endif

    @if (isset($report) && !empty($report))
        <input type="hidden" name="report" id="report" value="{{ $report }}">
    @else
        <div class="form-group">
            <label class="col-sm-3 control-label text-left">{{ __('cwp.reports.schedule.report') }}</label>
            <div class="col-sm-9">
                <select class="form-control" name="report" id="report">
                    <option value="">Select report</option>
                    @foreach($reports->data as $item)
                        <option value="{{ $item->uid }}"
                                @if (isset($schedule) && $schedule->_reportUid == $report->uid) selected @endif>{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    @endif

    <div class="form-group" id="schedule-starts">
        <label class="col-md-3 control-label">{{ __('cwp.reports.schedule.starts') }}</label>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" class="form-control" name="starts" id="starts"
                               value="{{ isset($schedule->scheduleDetails->startDateTime) ? Carbon\Carbon::parse($schedule->scheduleDetails->startDateTime)->toDateString() : date("Y-m-d") }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group clockpicker" data-autoclose="true">
                        <input type="text" class="form-control" name="starts-time" id="starts-time"
                               value="{{ isset($schedule->scheduleDetails->startDateTime) ? Carbon\Carbon::parse($schedule->scheduleDetails->startDateTime)->format('H:i') : '00:00' }}">
                        <span class="input-group-addon">
                        <span class="fa fa-clock-o"></span>
                    </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">{{ __('cwp.reports.schedule.frequency.title') }}</label>
        <div class="col-sm-9">
            <select class="form-control" name="frequency" id="frequency">
                @foreach($frequencies as $key => $frequency)
                    <option
                            @if (isset($schedule) && $schedule->scheduleDetails->frequency == $key)
                            selected
                            @elseif($loop->first)
                            selected
                            @endif value="{{ $key }}">
                        {{ $frequency }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group" id="schedule-ends" @if (!isset($schedule->scheduleDetails->frequency) || $schedule->scheduleDetails->frequency == 0) style="display: none" @endif>
        <label class="col-md-3 control-label">{{ __('cwp.reports.schedule.ends') }}</label>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" class="form-control" name="ends" id="ends"
                               value="{{ isset($schedule->scheduleDetails->endDateTime) && $schedule->scheduleDetails->endDateTime ? Carbon\Carbon::parse($schedule->scheduleDetails->endDateTime)->toDateString() : '' }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group clockpicker" data-autoclose="true">
                        <input type="text" class="form-control" name="ends-time" id="ends-time"
                               value="{{ isset($schedule->scheduleDetails->endDateTime) && $schedule->scheduleDetails->endDateTime ? Carbon\Carbon::parse($schedule->scheduleDetails->endDateTime)->format('H:i') : '' }}">
                        <span class="input-group-addon">
                        <span class="fa fa-clock-o"></span>
                    </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">{{ __('cwp.reports.schedule.delivery_method.title') }}</label>
        <div class="col-sm-9">
            <select class="form-control" name="delivery_method" id="delivery_method">
                @foreach($delivery_methods as $key => $method)
                    <option value="{{ $key }}"
                            @if (isset($schedule) && $schedule->deliveryMethod == $key) selected @endif>{{ $method }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div id="delivery">
        @if(isset($report) && $report)
            @include('pages.schedules.delivery-method')
        @endif
    </div>
    <input type="hidden" name="utc_offset" id="utc_offset" value="99">

</form>