@extends('layouts.cwp')

@section('title', __('cwp.reports.schedule.title'))

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('cwp.reports.title') }}
        </li>
        <li class="active">
            <strong>{{ __('cwp.reports.schedule.title') }}</strong>
        </li>
    </ol>
@endsection

@section('content')
    <div class="ibox-content m-b-md border-bottom p-top-xs-20 p-top-sm-20">
        <div class="row">
            <div class="col-md-2 col-xs-12 pull-right">
                <button type="button" class="btn btn-outline btn-primary btn-xs-block pull-right schedule-add">
                    <i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
                    {{ __("Add a schedule") }}
                </button>
            </div>
        </div>
    </div>

    <div id="table">
        @include('pages.schedules.table')
    </div>

    @include('pages.schedules.modal')
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            DTinit();
        });
    </script>
@endpush