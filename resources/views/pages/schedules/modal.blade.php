<div class="modal inmodal fade" id="schedule" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">{{ __('Schedule a report task') }}</h4>
            </div>
            <div class="modal-body">
                <div id="schedule-form-container">
                    @include('pages.schedules.form')
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit-schedule">Submit</button>
            </div>
        </div>
    </div>
</div>

@push('vendor-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js"></script>
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.clockpicker').clockpicker();

            $("#schedule-form-container").on('change', '#report', function () {
                $.ajax({
                    type: 'post',
                    url: '{{ url('reports') }}' + '/' + $('#report').val() + '/delivery',
                    success: function (data) {
                        $('#delivery').html(data);
                        switchDeliveryMethod($('#delivery_method').val());
                    },
                    error: function (data) {
                        $('#delivery').html('');
                    }
                });
            });

            $("#schedule-form-container").on('change', '#frequency', function () {
                if ($(this).val() == 0) {
                    switchEndDate(true)
                } else {
                    switchEndDate(false)
                }
            });

            $("#schedule-form-container").on('change', '#delivery_method', function () {
                switchDeliveryMethod($(this).val());
            });

            $(document).on('click', '.change-status', function (e) {
                $.ajax({
                    type: 'GET',
                    url: $(this).data('url'),
                    success: function (data) {
                        toastr.success(data.responseText);
                        getData();
                    },
                    error: function (data) {
                        toastr.error(JSON.parse(data.responseText));
                    }
                });
            });

            $(document).on('click', '.schedule-add', function (e) {
                $.ajax({
                    type: 'GET',
                    url: '{{ route('schedules.add') }}' + '/{{ $report ? $report : '' }}',
                    success: function (data) {
                        $('#schedule-form-container').html(data);
                        $('.clockpicker').clockpicker();
                        $('#utc_offset').val(getUTCOffset());
                        $('#schedule .date').datepicker({
                            keyboardNavigation: false,
                            forceParse: false,
                            format: "yyyy-mm-dd",
                            calendarWeeks: true,
                            autoclose: true
                        });
                        $('#schedule').modal('show');
                    },
                    error: function (data) {
                    }
                });
            });

            $(document).on('click', '.schedule-edit', function (e) {
                $.ajax({
                    type: 'GET',
                    url: $(this).data('edit-url'),
                    success: function (data) {
                        $('#schedule-form-container').html(data);
                        $('.clockpicker').clockpicker();
                        $('#utc_offset').val(getUTCOffset());
                        $('#schedule .date').datepicker({
                            keyboardNavigation: false,
                            forceParse: false,
                            format: "yyyy-mm-dd",
                            calendarWeeks: true,
                            autoclose: true
                        });
                        $('#schedule').modal('show');
                    },
                    error: function (data) {
                    }
                });
            });

            $('#schedule').on('click', '#submit-schedule', function (e) {
                $.ajax({
                    type: 'POST',
                    url: '{{ route('schedules.store') }}',
                    data: $("#schedule-form").serializeArray(),
                    dataType: 'json',
                    success: function (data) {
                        $('.error-note').remove();
                        $('.form-group').removeClass('has-error');
                        if (data.error) {
                            toastr.error(data.error, 'Something goes wrong!');
                        } else {
                            getData({'page': 1});
                            $('#schedule').modal('hide');
                            toastr.success(data.message, data.title);
                        }
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        $('.error-note').remove();
                        $('.form-group').removeClass('has-error');
                        $.each(errors, function (key, value) {
                            var field = $('#' + key);
                            var form_group = field.closest('.form-group');
                            form_group.addClass('has-error');
                            field.after('<span class="help-block m-b-none error-note">' + value + '</span>');
                        });
                    }
                });
            });

            $('.modal').on('hidden.bs.modal', function (e) {
                $('#schedule-form-container').html('');
            });

            $('#schedule .date').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                format: "yyyy-mm-dd",
                calendarWeeks: true,
                autoclose: true
            });
        });

        function switchEndDate(hide) {
            if (hide) {
                $('#schedule-ends').hide();
            } else {
                $('#schedule-ends').show();
            }
        }

        function switchDeliveryMethod(method) {
            if (parseInt(method) === 1) {
                $('#to_emails').hide();
                $('#to_endpoint').show();
            } else {
                $('#to_endpoint').hide();
                $('#to_emails').show();
            }
        }

        function getUTCOffset(){
            var d = new Date();
            var offset = d.getTimezoneOffset();
            return offset;
        }
    </script>
@endpush