@component('partials.table', compact('data', 'pagination'))
    <table class="table table-striped table-bordered table-hover" id="dataTable">
        <thead>
        <tr>
            <th>{{ $data->columns->name->displayName }}</th>
            <th>{{ $data->columns->deliveryMethod->displayName }}</th>
            <th>{{ $data->columns->deliveryData->displayName }}</th>
            <th>{{ __('Frequency') }}</th>
            <th>{{ __('Start date') }}</th>
            <th>{{ __('End date') }}</th>
            <th>{{ $data->columns->status->displayName }}</th>
            <th style="width: 125px"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data->data as $schedule_data)
            <tr>
                <td>{{ $schedule_data->name }}</td>
                <td class="schedule_delivery_method">{{ $delivery_methods[$schedule_data->deliveryMethod] }}</td>
                <td class="schedule_delivery_data">
                    @if ($schedule_data->deliveryMethod == 1)
                        {{ isset($endpoints[$schedule_data->deliveryData]->name) ? $endpoints[$schedule_data->deliveryData]->name
                         : $schedule_data->deliveryData}}
                    @else
                        @php
                            $emails = explode(';', $schedule_data->deliveryData);
                            $emails = implode(',', $emails);
                        @endphp
                        {{ $emails }}
                    @endif
                </td>

                <td class="schedule_delivery_frequency">
                    {{ $frequencies[$schedule_data->scheduleDetails->frequency] }}
                </td>
                <td class="schedule_delivery_starts">
                    {{ Carbon\Carbon::parse($schedule_data->scheduleDetails->startDateTime)
                    ->subMinutes($timezone_offset)->format('Y-m-d g:i a') }}
                </td>


                <td class="schedule_delivery_ends">
                    @if ($schedule_data->scheduleDetails->endDateTime)
                        {{ Carbon\Carbon::parse($schedule_data->scheduleDetails->endDateTime)
                        ->subMinutes($timezone_offset)->format('Y-m-d g:i a') }}
                    @endif
                </td>

                <td class="schedule_status">{{ $schedule_data->status or 'n/a' }}</td>
                <td class="text-center">
                    <div style="width: 125px">
                    @if ($schedule_data->status == 'Active')
                        <button class="btn btn-primary btn-outline schedule-edit" title="{{ __('Edit') }}"
                                data-edit-url="{{ route('schedules.edit', [$schedule_data->_uid]) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </button>
                        <button data-url="{{ route('schedules.change-status', ['uid' => $schedule_data->_uid, 'status' => 'Inactive' ]) }}"
                                class="btn btn-warning btn-outline change-status" title="{{ __('Deactivate') }}">
                            <i class="fa fa-power-off" aria-hidden="true"></i>
                        </button>
                    @elseif ($schedule_data->status == 'Inactive')
                        <button class="btn btn-primary btn-outline schedule-edit" title="{{ __('Edit') }}"
                                data-edit-url="{{ route('schedules.edit', [$schedule_data->_uid]) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </button>
                        <button data-url="{{ route('schedules.change-status', ['uid' => $schedule_data->_uid, 'status' => 'Active' ]) }}"
                                class="btn btn-primary btn-outline change-status" title="{{ __('Activate') }}">
                            <i class="fa fa-power-off" aria-hidden="true"></i>
                        </button>
                        <button data-url="{{ route('schedules.change-status', ['uid' => $schedule_data->_uid, 'status' => 'Deleted' ]) }}"
                                class="btn btn-danger btn-outline change-status" title="{{ __('Delete') }}">
                            <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                        </button>
                    @elseif ($schedule_data->status == 'Deleted')

                    @else
                        <button data-url="{{ route('schedules.change-status', ['uid' => $schedule_data->_uid, 'status' => 'Deleted' ]) }}"
                                class="btn btn-danger btn-outline change-status" title="{{ __('Delete') }}">
                            <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                        </button>
                    @endif
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endcomponent