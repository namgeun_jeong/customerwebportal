<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{ asset('favicon.ico?v=2') }}" />
    <title>Login</title>
    <style>
        .auth0-lock.auth0-lock .auth0-lock-header-bg {
            background-color: #FFFFFF !important;
        }

        .auth0-lock-header-bg-blur, .auth0-lock-header-bg-solid {
            opacity: 0 !important;
        }

        .auth0-lock-header {
            border-bottom: 5px solid #142d52;
        }
    </style>
</head>
<body style="background: #FFFFFF">

<div id="root" style="padding: 40px 0"></div>

<script src="https://cdn.auth0.com/js/lock/10.16/lock.min.js"></script>
<script>
    var lock = new Auth0Lock('{{ config('laravel-auth0.client_id') }}', '{{ config('laravel-auth0.domain') }}', {
        container: 'root',
        auth: {
            redirectUrl: '{{ config('laravel-auth0.redirect_uri') }}',
            responseType: 'code',
            params: {
                scope: 'openid email app_metadata' // Learn about scopes: https://auth0.com/docs/scopes
            }
        },
        theme: {
            primaryColor: '#142d52',
            logo: "{{ asset('images/logo.png') }}",
        },
        languageDictionary: {
            title: ""
        },
    });
    lock.show();
</script>

</body>
</html>