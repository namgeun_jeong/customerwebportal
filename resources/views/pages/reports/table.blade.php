@if (isset($data->searchFields) and !empty($data->searchFields))
    @include('partials.advanced-search')
@endif

@component('partials.table', compact('data', 'pagination'))
    <table class="table table-striped table-bordered table-hover" id="dataTable">
        <thead>
        <tr>
            @foreach($data->columns as $column)
                <th @if ($column->maxWidth) style="width: {{ $column->maxWidth }}" @endif>
                    <div data-toggle="tooltip" data-placement="top" title="{{ $column->tooltips }}">
                        {{ $column->displayName }}
                    </div>
                </th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($data->data as $row)
            <tr>
                @foreach(get_object_vars($data->columns) as $key => $value)
                    @if ($key == 'date')
                        <td>{{ Carbon\Carbon::parse($row->{$key})->toDateTimeString() }}</td>
                    @elseif (is_numeric($row->{$key}))
                        <td>
                            @if (strpos($row->{$key}, "." ) !== false)
                                {{ $row->{$key} }}
                            @else
                                {{ number_format( $row->{$key} , 0 , '.' , ',' ) }}
                            @endif
                        </td>
                    @else
                        <td>{{ $row->{$key} }}</td>
                    @endif
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
@endcomponent