@extends('layouts.cwp')

@section('title', request()->name)

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            {{ __('cwp.reports.title') }}
        </li>
        <li class="active">
            <strong>{{ __('Report data') }}</strong>
        </li>
    </ol>
@endsection

@section('content')
    @if (\App\User::hasRole('ReportsManager'))
        <div class="ibox-content m-b-md border-bottom p-top-xs-20 p-top-sm-20">
            <div class="row">
                <div class="col-md-2 col-xs-12 pull-right">
                    <a href="{{ route('reports.schedules', ['report' => $report]) }}"
                       class="btn btn-outline btn-primary btn-xs-block pull-right">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        {{ __("Schedules") }}
                    </a>
                </div>
            </div>
        </div>
    @endif

    <div id="table">
        @include('pages.reports.table')
    </div>
@endsection

@push('scripts')
    <script>
        var full_width = true;

        $(document).ready(function () {
            DTinit();
        });
    </script>
@endpush