<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ config('app.url') }}">

    <link rel="icon" href="{{ asset('favicon.ico?v=3') }}"/>

    <title>
        @hasSection ('title')
            @yield('title')
        @else
            {{ config('app.name', 'Laravel') }}
        @endif
    </title>

    <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

    <link href="{{ asset('css/vendor.css') }}" rel="stylesheet">

    @stack('vendor-css')

    <link href="{{ asset('css/app.css?time=' . time()) }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    @if (config('cwp.analytics.hotjar'))
        <script>
            (function (h, o, t, j, a, r) {
                h.hj = h.hj || function () {
                    (h.hj.q = h.hj.q || []).push(arguments)
                };
                h._hjSettings = {hjid: 797085, hjsv: 6};
                a = o.getElementsByTagName('head')[0];
                r = o.createElement('script');
                r.async = 1;
                r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
                a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
    @endif

    @if (config('cwp.analytics.google'))
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114908735-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());
            gtag('config', 'UA-114908735-1');
        </script>
    @endif
</head>

<body>

<div id="wrapper">

    @include('partials.menus.left')

    <div id="page-wrapper" class="gray-bg">

        @include('partials.menus.top')

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-12 col-md-8">
                <h2>
                    @yield('title')
                </h2>
                @yield('breadcrumb')
            </div>

            <div class="visible-md-block visible-lg-block">
                <div class="col-md-4 customer-logo">
                    @if ($customerLogo)
                        <img src="{{ $customerLogo }}" class="img-responsive pull-right" alt="{{ $customerName }}"
                             title="{{ $customerName }}">
                    @endif
                </div>
            </div>
        </div>

        <div class="row wrapper wrapper-content animated fadeInRight">
            @yield('content')
        </div>

        @include('partials.footer')

    </div>
</div>

<!-- Mainly scripts -->
<script src="{{ asset('js/app.js?time=' . time()) }}"></script>
<script src="{{ asset('js/vendor.js') }}"></script>


@stack('vendor-scripts')

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function initAdvancedSearch() {
        $(".touchspin").TouchSpin({
            buttondown_class: 'btn btn-white',
            buttonup_class: 'btn btn-white'
        });

        $('.date').datepicker({
            format: "yyyy-mm-dd",
        });

        $('.clockpicker').clockpicker();

        $('.field-select').chosen({width: "100%"});
    }

    function getData(data) {

        $('.ibox.table').children('.ibox-content').toggleClass('sk-loading');

        if (typeof data == 'undefined') {
            data = getOptions();
        }

        $.ajax({
            url: '{{ url()->current() }}',
            method: "POST",
            dataType: 'html',
            data: data,
            success: function (data) {
                $('#dataTable').dataTable().fnDestroy();
                $('#table').html(data);
                DTinit();

                if (typeof initAdvancedSearch == 'function') {
                    initAdvancedSearch();
                }
            },
            error: function (data) {
                alert('Data could not be loaded.');
            }
        });
    }

    function initDTSearchFields() {

        var search_selector = $('#dataTable thead th');
        var columnDefs = [];
        var search_row = '<thead class="filters"><tr>';

        if (typeof full_width == 'undefined') {
            search_selector = search_selector.not(':last');
            columnDefs = [
                {"searchable": false, 'sortable': false, "targets": -1},
            ];
        }

        if (typeof offset_first != 'undefined') {
            search_row += '<td></td>';
            search_selector = search_selector.not(':first');
            columnDefs.push({"searchable": false, 'sortable': false, "targets": 0});
        }

        search_selector.each(function () {
            search_row += '<td><input type="text" class="form-control" style="  width: 100%;"/></td>';
        });

        if (typeof full_width == 'undefined') {
            search_row += '<td></td>';
        }

        search_row += '</thead></tr>';

        $('#dataTable thead').after(search_row);

        return columnDefs;
    }

    function DTIndividualSearch(table) {
        if ($('#dataTable').length) {
            table.columns().eq(0).each(function (colIdx) {
                $($('.filters td')[colIdx]).on('keyup change', 'input', function () {
                    table
                        .column(colIdx)
                        .search(this.value)
                        .draw();
                });
            });
        }
    }

    function DTinit() {

        var columnDefs = initDTSearchFields();

        var table = $('#dataTable').DataTable({
            "order": [],
            "pageLength": '{{ isset($data->paging->itemsPerPage) ? $data->paging->itemsPerPage : 0 }}',
            "dom": 't',
            "autoWidth": false,
            drawCallback: function (settings) {
                var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
                pagination.toggle(this.api().page.info().pages > 1);
            },
            "columnDefs": columnDefs
        });

        DTIndividualSearch(table);

        $('[data-toggle="tooltip"]').tooltip();

    }

    function getOptions() {
        var data = {};

        if ($("#date_from").length) {
            data.date_from = $("#date_from").val();
        }

        if ($("#date_to").length) {
            data.date_to = $("#date_to").val();
        }

        if ($('#advanced-search-form').length) {
            data.searchFields = $('#advanced-search-form').serializeArray();
        }

        if ($('.chosen-select.verticals').length) {
            data.vertical = $('.chosen-select.verticals').val();
        }

        if ($('.chosen-select.products').length) {
            data.products = $('.chosen-select.products').val();
        }

        if ($('.chosen-select.statuses').length) {
            data.statuses = $('.chosen-select.statuses').val();
        }

        if ($('.chosen-select.actions').length) {
            data.actions = $('.chosen-select.actions').val();
        }

        return data;
    }

    $(document).ready(function () {

        $('.customer-select').chosen();

        $('.customer-select').on('change', function (e) {
            location.href = '{{ config('app.url') }}/customers/switch/' + this.value;
            return false;
        });

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        @if(Session::has('message'))
            var type = "{{ Session::get('alert-type', 'info') }}";
            switch (type) {
                case 'info':
                    toastr.info("{{ Session::get('message') }}");
                    break;

                case 'warning':
                    toastr.warning("{{ Session::get('message') }}");
                    break;

                case 'success':
                    toastr.success("{{ Session::get('message') }}");
                    break;

                case 'error':
                    toastr.error("{{ Session::get('message') }}");
                    break;
            }
        @endif

        $(document).on('click', '#search', function (e) {
            getData();
        });

        $(document).on('click', '#clear', function (e) {
            $('#advanced-search-form')[0].reset();
            getData({});
        });

        $('.collapsed', document).on('click', '.close-link', function () {
            var content = $(this).closest('div.ibox');
            content.remove();
        });

        @if (!isset($timezone_offset))
        var offset = (new Date()).getTimezoneOffset();
        $.ajax({
            type: 'POST',
            url: '{{ route('users.update-timezone') }}',
            data: {timezone_offset: offset},
            success: function (data) {
            },
            error: function (data) {
            }
        });
        @endif

    });
</script>

@if (!request()->is('users'))
<script>
    $(document).on('click', '.pagination a', function (e) {
        let url_string = $(this).attr('href');
        let url = new URL(url_string);
        let page = url.searchParams.get("page");

        let data = getOptions();
        data.page = page;

        getData(data);

        e.preventDefault();
    });
</script>
@endif

@stack('scripts')

</body>
</html>
