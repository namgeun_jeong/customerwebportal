@if (isset($customer->logo))
    <div class="col-sm-8 customer-logo">
        <img src="{{ $customer->logo }}" class="img-responsive pull-right">
    </div>
@endif