<div class="ibox m-b-md border-bottom">
    <div class="ibox-title">
        <h5>
            <i class="fa fa-search-plus" aria-hidden="true"></i>
            {{ __('Advanced search') }}
        </h5>
    </div>
    <div class="ibox-content p-bottom-xs-0 p-bottom-sm-0">
        <form id="advanced-search-form">
            <div class="row">
                @foreach($data->searchFields as $field)
                    <div class="col-sm-3">
                        <div class="form-group">
                            @if (isset($field->inputType))

                                @if ($field->inputType === 0) {{--Text field--}}

                                {{--<label class="control-label">{{ $field->name }}</label>--}}
                                <input name="{{ $field->name }}" value="{{ ucfirst($field->selectedValue) }}"
                                       class="form-control" placeholder="{{ ucfirst($field->name) }}">

                                @elseif($field->inputType === 1) {{--Number field--}}

                                {{--<label class="control-label">{{ $field->name }}</label>--}}
                                <input name="{{ $field->name }}" value="{{ $field->selectedValue }}"
                                       class="touchspin form-control" placeholder="{{ ucfirst($field->name) }}">

                                @elseif($field->inputType === 2){{--Combo  field--}}

{{--                                <label class="control-label">{{ $field->name }}</label>--}}
                                <select data-placeholder="{{ ucfirst($field->name) }}" class="field-select" multiple style="width:350px;"
                                        tabindex="4" name="{{ $field->name }}">
                                    @if (!empty($field->displayValue))
                                        @foreach($field->displayValue as $value)
                                            @if (!empty($field->selectedValue) && in_array($value, json_decode($field->selectedValue)))
                                                <option value="{{ $value }}" selected>
                                            @else
                                                <option value="{{ $value }}">
                                            @endif
                                                    {{ $value }}
                                                </option>
                                        @endforeach
                                    @endif
                                </select>

                                @elseif($field->inputType === 3){{--Date field--}}

                                {{--<label class="control-label">{{ $field->name }}</label>--}}
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input value="{{ $field->selectedValue ? Carbon\Carbon::parse($field->selectedValue)->toDateString() : '' }}"
                                           class="form-control" placeholder="{{ ucfirst($field->name) }}">
                                    <input type="hidden" class="date-field" name="{{ $field->name }}"
                                           value="{{ $field->selectedValue }}">
                                </div>

                                @elseif($field->inputType === 4){{--Time field--}}
                                {{--<label class="control-label">{{ $field->name }}</label>--}}
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <input value="{{ $field->selectedValue ? Carbon\Carbon::parse($field->selectedValue)->format('H:i') : '' }}"
                                           class="form-control" placeholder="{{ ucfirst($field->name) }}">
                                    <input type="hidden" class="time-field" name="{{ $field->name }}"
                                           value="{{ $field->selectedValue ? Carbon\Carbon::parse($field->selectedValue)->format('H:i') : '' }}">
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>

                                @endif
                            @else
                                {{--<label class="control-label">{{ $field->name }}</label>--}}
                                <input name="{{ $field->name }}" value="{{ $field->selectedValue }}"
                                       class="form-control" placeholder="{{ ucfirst($field->name) }}">
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </form>
    </div>
    <div class="ibox-footer">
        <button class="btn btn-outline btn-xs-block btn-success" type="button" id="search">
            <i class="fa fa-search" aria-hidden="true"></i>
            {{ __('Search') }}
        </button>
        <button type="button" class="btn btn-outline btn-xs-block btn-warning" id="clear">
            <i class="fa fa-times" aria-hidden="true"></i>
            {{ __('Clear') }}
        </button>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {
            initAdvancedSearch();
        });

        function initAdvancedSearch() {
            $(".touchspin").TouchSpin({
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $('.date').datepicker({
                format: "yyyy-mm-dd",
            });

            $('.clockpicker').clockpicker();

            $('.field-select').chosen({width: "100%"});
        }
    </script>
@endpush