@if (isset($pagination))
    <div class="row">
        @if (isset($data->paging->totalItems))
            <div class="table-info">
                <b>{{ number_format($data->paging->totalItems) }}</b>
                Total
                {{ str_plural('item', $data->paging->totalItems)  }}
            </div>
        @endif

        {!! $pagination->appends(request()->except('page'))->render() !!}
    </div>
@endif