<div class="footer">
    <div class="text-center">
        <strong>Copyright</strong> <a href="http://abcorp.com/">ABCorp</a> - {{ config('app.name') }} &copy; 2017 - {{ date("Y") }}
    </div>
</div>