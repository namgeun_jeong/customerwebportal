<div class="row">
    <div class="col-lg-12">
        <div class="ibox table">
            <div class="ibox-content border-bottom table-data p-top-xs-0 p-top-sm-0 p-bottom-xs-0 p-bottom-sm-0">
                <div class="sk-spinner sk-spinner-wave">
                    <div class="sk-rect1"></div>
                    <div class="sk-rect2"></div>
                    <div class="sk-rect3"></div>
                    <div class="sk-rect4"></div>
                    <div class="sk-rect5"></div>
                </div>

                @if (isset($data->data) && !empty($data->columns))
                    @include('partials.pagination')
                    <div class="table-responsive">
                        {{ $slot }}
                    </div>
                    <div class="p-bottom-xs-0 p-bottom-sm-0">
                        @include('partials.pagination')
                    </div>
                @else
                    <div class="alert alert-warning m-t-md m-b-md">
                        {{ __('No data available.') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>