<style>
    body {
        background: {{ $theme['menuMain'] }};
    }

    body.mini-navbar .nav-header {
        background-color: {{ $theme['menuMain'] }};
    }

    .mini-navbar .nav .nav-second-level {
        background: {{ $theme['menuActive'] }};
    }

    .nav > li > a {
        color: {{ $theme['main'] }};
    }

    .nav-header {
        background-image: none;
        background: {{ $theme['menuMain'] }};
    }

    .nav > li.active {
        border-left: 4px solid {{ $theme['menuMain'] }};
        background: {{ $theme['menuActive'] }};
    }

    .page-heading h2 {
        color: {{ $theme['main'] }};
    }

    ul.nav-second-level {
        background: {{ $theme['menuMain'] }};
    }

    .navbar-default .nav > li > a:hover, .navbar-default .nav > li > a:focus {
        background-color: {{ $theme['menuActive'] }};
    }

    {{--.btn-primary {--}}
        {{--background-color: {{ $theme['main'] }};--}}
        {{--border-color: {{ $theme['main'] }} ;--}}
        {{--color: #FFFFFF;--}}
    {{--}--}}

    #order-details-modal .modal-title {
        color: {{ $theme['main'] }};
    }

    .text-cwp {
        color: {{ $theme['main'] }};
    }

    {{--.btn-link:hover, .btn-link:focus, .btn-link:active, .btn-link.active, .open .dropdown-toggle.btn-link {--}}
        {{--color: {{ $theme['main'] }};--}}
    {{--}--}}

    {{--.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary, .btn-primary:active:focus, .btn-primary:active:hover, .btn-primary.active:hover, .btn-primary.active:focus {--}}
        {{--background-color: {{ $theme['main'] }};--}}
        {{--border-color: {{ $theme['main'] }} ;--}}
    {{--}--}}

    .clockpicker-popover {
        z-index: 10000;
    }
</style>