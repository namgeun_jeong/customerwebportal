<div class="form-group m-n" id="date-range">
    <div class="input-daterange input-group" id="datepicker">
        <input class="input-sm form-control" name="date_from" id="date_from"
               value="{{ (isset($date_from) && $date_from) ? $date_from : Carbon\Carbon::now()->subDays(28)->toDateString() }}"/>
        <span class="input-group-addon">to</span>
        <input class="input-sm form-control" name="date_to" id="date_to"
               value="{{ (isset($date_to) && $date_to) ? $date_to : Carbon\Carbon::now()->toDateString() }}"/>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.input-daterange input').each(function () {
                $(this).datepicker({
                    keyboardNavigation: false,
                    format: "yyyy-mm-dd",
                    forceParse: false,
                    autoclose: true
                }).on("changeDate", function (e) {
                    getData();
                });
            });
        });
    </script>
@endpush