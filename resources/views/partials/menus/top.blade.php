<div class="row border-bottom">
    <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#"><i class="fa fa-bars"></i> </a>
        </div>

        @if (isset($customers) && count($customers))
            <ul class="nav navbar-top-links navbar-left">
                <li class="dropdown select-customer">
                    <select data-placeholder="Choose a customer..." class="customer-select" style="padding: 20px 10px">
                        @foreach($customers as $dataPrepID => $customer)
                            <option value="{{ $dataPrepID }}" @if ($customerName == $dataPrepID) selected @endif>
                                {{ $customer }}
                            </option>
                        @endforeach
                    </select>
                </li>
            </ul>
        @endif

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">
                    Welcome to <a href="http://abcorp.com/">ABCorp</a> {{ config('app.name') }}
                </span>
            </li>
            <li class="hidden-xs">
                <a href="{{ route('logout') }}">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>