<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">

            <li class="nav-header">
                <div class="dropdown profile-element text-center">
                    <span>
                        @if (isset(auth()->user()->getUserInfo()['user_metadata']['picture']))
                            <img alt="image" class="img-responsive text-center"
                                 src="{{ auth()->user()->getUserInfo()['user_metadata']['picture'] }}"
                                 style="margin: 0 auto;"/>
                        @else
                            <img alt="image" class="img-responsive text-center" src="{{ auth()->user()->picture }}"
                                 style="margin: 0 auto;"/>
                        @endif

                    </span>

                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ auth()->user()->nickname }}</strong>
                            </span>
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                    CWP
                </div>
            </li>

            @if (\App\User::roleIn(['Admin', 'ABCorp Admin']))
                <li @if (in_array(request()->segment(1), ['users'])) class="active" @endif>
                    <a href="#">
                        <i class="fa fa-shield" aria-hidden="true"></i>
                        <span class="nav-label">{{ __('Administration') }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li @if (in_array(request()->segment(1), ['users'])) class="active" @endif>
                            <a href="{{ route('users.index') }}">
                                <i class="fa fa-user-circle" aria-hidden="true"></i>
                                {{ __('Users') }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif

            @if (\App\User::roleIn(['AccountManager', 'ABCorp Account Manager', 'ABCorp Admin']))
                <li @if (in_array(request()->segment(1), ['management'])) class="active" @endif>
                    <a href="{{ route('management.index') }}">
                        <i class="fa fa-cube" aria-hidden="true"></i>
                        <span class="nav-label">{{ __('Product and Parts') }}</span>
                    </a>
                </li>
            @endif

            @if (\App\User::roleIn(['ABCorp Account Manager', 'ABCorp Admin', 'OrderUser', 'OrderManager', 'RequestUser', 'RequestManager']))
                <li class="landing_link">
                    <a href="{{ route('products.data') }}">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span class="nav-label">Search</span>
                    </a>
                </li>

                <li @if (in_array(request()->segment(1), ['orders', 'work', 'products', 'requests', ])) class="active" @endif>
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span class="nav-label">{{ __('cwp.orders.title') }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li @if (request()->routeIs('customers.data') or request()->routeIs('rejected.data')) class="active" @endif>
                            <a href="{{ route('customers.data') }}">{{ __('Customer Orders') }}</a>
                        </li>

                        <li @if (request()->routeIs('work.data') or
                                request()->routeIs('order-items.order') or
                                request()->routeIs('work.show') or
                                request()->routeIs('work.customer')) class="active" @endif>
                            <a href="{{ route('work.data') }}">{{ __('Work Orders') }}</a>
                        </li>

                        <li @if (request()->routeIs('products.data')) class="active" @endif>
                            <a href="{{ route('products.data') }}">{{ __('Product Items') }}</a>
                        </li>

                        @if (\App\User::roleIn(['ABCorp Account Manager', 'ABCorp Admin', 'OrderManager', 'OrderUser', 'RequestUser', 'RequestManager']))
                            <li @if (request()->routeIs('requests.data') or request()->routeIs('requests.show')) class="active" @endif>
                                <a href="{{ route('requests.data') }}">{{ __('Customer Requests') }}</a>
                            </li>
                        @endif
                    </ul>
                </li>

            @endif

            @if (\App\User::roleIn(['ABCorp Account Manager', 'ABCorp Admin', 'OrderManager', 'OrderUser', 'ReportUser', 'ReportManager']))
                <li @if (request()->segment(1) == 'reports') class="active" @endif>
                    <a href="#">
                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        <span class="nav-label">{{ __('cwp.reports.title') }}</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse">
                        @if (isset($reports->data) && !empty($reports->data))
                            @foreach($reports->data as $report)
                                <li @if (request()->segment(2) === $report->uid) class="active" @endif>
                                    <a href="{{ route('reports.data', ['report' => $report->uid, 'name' => $report->name]) }}">{{ $report->name }}</a>
                                </li>
                            @endforeach
                        @endif

                        @if (\App\User::roleIn(['ABCorp Account Manager', 'ABCorp Admin', 'Client Admin']))
                            <li @if (request()->routeIs('schedules.data') ) class="active" @endif>
                                <a href="{{ route('schedules.data') }}">{{ __('cwp.reports.schedule.title') }}</a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

            <li class="special_link visible-xs-inline-block">
                <a href="{{ route('logout') }}">
                    <i class="fa fa-sign-out"></i>
                    <span class="nav-label">Log out</span>
                </a>
            </li>

        </ul>

        <div class="logo">
            <a href="http://www.abnote.com">
                <img src="{{ asset('images/logo_white.png') }}" class="img-responsive">
            </a>
        </div>
    </div>
</nav>