<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\CWP::class, function (Faker\Generator $faker) {
    return [
//        '_uid' => $faker->unique()->numberBetween(1, 1000),
//        'date' => $faker->iso8601(),
//        'orderID' => $faker->word,
//        'source' => $faker->randomElement(['SFTP', 'WEB']),
//        'totalQuantity' => $faker->numberBetween(50, 1000),
//        'rejectedQuantity' => $faker->numberBetween(1, 20),
//        'destroyedQuantity' => $faker->numberBetween(1, 20),
//        'productionQuantity' => $faker->numberBetween(1, 50),
//        "#workOrder" => $faker->boolean,
//        "#cancel" => $faker->boolean,

        $faker->unique()->numberBetween(1, 1000),
        $faker->iso8601(),
        $faker->word,
        $faker->randomElement(['SFTP', 'WEB']),
        $faker->numberBetween(50, 1000),
        $faker->numberBetween(1, 20),
        $faker->numberBetween(1, 20),
        $faker->numberBetween(1, 50),
        $faker->boolean,
        $faker->boolean,
    ];
});
