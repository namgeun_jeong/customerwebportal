<?php

namespace App\Providers;


use App\ABCorp\CWP\API\Auth0\GroupsRepository;
use App\ABCorp\CWP\API\Auth0\GroupsRepositoryInterface;
use App\ABCorp\CWP\API\Auth0\RolesRepository;
use App\ABCorp\CWP\API\Auth0\RolesRepositoryInterface;
use App\ABCorp\CWP\API\Auth0\UserRepository;
use App\ABCorp\CWP\API\Auth0\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bind the interface to an implementation repository class
     */
    public function register()
    {
        $this->app->bind(
            GroupsRepositoryInterface::class,
            GroupsRepository::class
        );

        $this->app->bind(
            RolesRepositoryInterface::class,
            RolesRepository::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
    }
}