<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            [
                'layouts.cwp',
                'partials.menus.top',
                'partials.customer-logo',
            ], 'App\Http\ViewComposers\CustomerComposer'
        );

        View::composer(
            'partials.menus.left', 'App\Http\ViewComposers\ReportsListComposer'
        );

        View::composer(
            [
                'pages.orders.items.actions.reroute',
                'pages.orders.requests.details',
                'pages.orders.requests.show',
            ], 'App\Http\ViewComposers\CountriesComposer'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}