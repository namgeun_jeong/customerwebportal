<?php

namespace App\Traits;


use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

trait Pagination
{
    public function makePagination($data, Request $request)
    {
        $page = isset($request->page) ? $request->page : $data->paging->currentpage + 1;
        $perPage = $data->paging->itemsPerPage;

        return new LengthAwarePaginator([], $data->paging->totalItems, $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);
    }

    public function getCurrentPage(Request $request)
    {
        return isset($request->page) ? $request->page - 1 : 0;
    }
}