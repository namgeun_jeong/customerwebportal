<?php


namespace App\Traits;


trait Configuration
{
    public function searchMetadata($groups, $search)
    {
        return array_where($groups, function ($value, $key) use ($search) {
            return in_array($key, $search);
        });
    }
}