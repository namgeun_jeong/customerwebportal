<?php

namespace App\Traits;


use Carbon\Carbon;

trait AdvancedSearch
{
    /**
     * Get search fields from request.
     *
     * @param $apiSearchFields
     * @return array|null
     */
    public function getSearchFields($apiSearchFields)
    {
        if (!isset(request()->searchFields)) {
            return null;
        }

        $searchFields = array_where(request()->searchFields, function ($value, $key) {
            return (bool)$value['value'];
        });

        $data = [];

        foreach ($apiSearchFields as $apiSearchField) {
            $data[$apiSearchField->name] = $apiSearchField;
        }

        $result = [];

        foreach ($searchFields as $key => $searchField) {

            switch ($data[$searchField['name']]->inputType) {
                case 1: // Number field
                    $result[$searchField['name']] = [
                        'name'          => $searchField['name'],
                        'selectedValue' => (int)$searchField['value'],
                    ];
                    break;
                case 2: // Combo  field
                    if (isset($result[$searchField['name']])) {
                        array_push($result[$searchField['name']]['selectedValue'], $searchField['value']);
                    } else {
                        $result[$searchField['name']] = [
                            'name'          => $searchField['name'],
                            'selectedValue' => [$searchField['value']],
                        ];
                    }
                    break;
                case 3: // Date field
                    $result[$searchField['name']] = [
                        'name'          => $searchField['name'],
                        'selectedValue' => Carbon::parse($searchField['value'])->toDateString() . 'T00:00:00.000',
                    ];
                    break;
                case 4: // Time field
                    $result[$searchField['name']] = [
                        'name'          => $searchField['name'],
                        'selectedValue' => Carbon::now()->toDateString() . 'T'.$searchField['value'].':00.000',
                    ];
                    break;
                default: // Text field
                    $result[$searchField['name']] = [
                        'name'          => $searchField['name'],
                        'selectedValue' => $searchField['value'],
                    ];
                    break;
            }

            $result[$searchField['name']]['inputType'] = $data[$searchField['name']]->inputType;
        }

        foreach ($result as $key => $item) {
            if (is_array($result[$key]['selectedValue'])) {
                $result[$key]['selectedValue'] = json_encode($result[$key]['selectedValue']);
            }
        }

        return array_values($result);
    }

    /**
     * Is results include active search parameters.
     *
     * @param $searchFields
     * @return bool
     */
    public function isIncludeSearch($searchFields)
    {
        $search_fields = array_where($searchFields, function ($value, $key) {
            return $value->selectedValue !== null;
        });

        return !empty($search_fields);
    }

    public function getStartDate($request, $with_time = false)
    {
        $date = isset($request->date_from) ? $request->date_from : Carbon::now()->subDays(28)->toDateString();
        return $with_time ? $date . 'T00:00:00.000' : $date;
    }

    public function getEndDate($request, $with_time = false)
    {
        $date = isset($request->date_to) ? $request->date_to : Carbon::now()->toDateString();
        return $with_time ? $date . 'T23:59:59.000' : $date;
    }

    public function getSelectedStatuses($request)
    {
        return (isset($request->selectedStatuses) && count($request->selectedStatuses)) ? $request->selectedStatuses : [];
    }

    public function getSelectedProducts($request, $single = false)
    {
        return (isset($request->selectedProducts) && count($request->selectedProducts)) ? $request->selectedProducts : [];
    }
}