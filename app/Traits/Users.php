<?php

namespace App\Traits;


trait Users
{
    /**
     * Filter users by group.
     *
     * @param $users
     * @param $group
     * @return array
     */
    public function getByGroup($users, $group): array
    {
        return array_where($users, function ($value, $key) use ($group) {
            $groups = $this->getGroups($value);

            if (empty($groups)) {
                return false;
            }

            return in_array($group, $groups);
        });
    }

    /**
     * Get groups that user belongs to.
     *
     * @param $user
     * @return array
     */
    public function getGroups($user)
    {
        return $user->app_metadata->authorization->groups ?? [];
    }

    /**
     * Get user roles.
     *
     * @param $user
     * @return array
     */
    public function getRoles($user): array
    {
        return $user->app_metadata->authorization->roles ?? [];
    }

    /**
     * Determine if the user belongs to the group.
     *
     * @param $user
     * @param $group
     * @return bool
     */
    public function hasGroup($user, $group): bool
    {
        return in_array($group, $this->getGroups($user));
    }

    /**
     * Determine if the user exist.
     *
     * @param $user
     * @return bool
     */
    public function valid($user): bool
    {
        return isset($user->email);
    }
}