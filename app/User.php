<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    private static $roles = [
        "ABCorp Account Manager",
        "ABCorp Admin",
        "Account Manager",
        "Admin",
        "Client Admin",
        "OrderUser",
        "OrderManager",
        "ReportUser",
        "ReportManager",
        "RequestUser",
        "RequestManager"
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return array
     */
    public static function getRoles(): array
    {
        return static::$roles;
    }

    /**
     * Check user role.
     *
     * @param string $role
     * @return bool
     */
    public static function hasRole(string $role)
    {
        if (self::isAdmin()) {
            return true;
        }

        if (array_search($role, auth()->user()->getUserInfo()['roles']) !== false) {
            return true;
        }

        return false;
    }

    private static function isAdmin()
    {
        if (array_search('Admin', auth()->user()->getUserInfo()['roles']) !== false) {
            return true;
        }

        return false;
    }

    public static function roleIn(array $roles)
    {
        if (self::isAdmin()) {
            return true;
        }

        foreach ($roles as $role) {
            if (in_array($role, auth()->user()->getUserInfo()['roles'])) {
                return true;
            }
        }

        return false;
    }
}
