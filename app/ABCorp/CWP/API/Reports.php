<?php


namespace App\ABCorp\CWP\API;
use Log;
use DateTime;
use DateInterval;

class Reports extends CustomerWebPortal
{
    private $frequencies = [
        'None', 'Daily', 'Weekly', 'Fortnightly', 'Monthly', 'Quarterly', 'Yearly',
    ];

    private $delivery_methods = [
        'Email', 'Sftp',
    ];

    private $statuses = [
        'Active', 'Inactive', 'Deleted',
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function getFrequencies(bool $keys = false)
    {
        return $keys ? implode(',', array_keys($this->frequencies)) : $this->frequencies;
    }

    public function getDeliveryMethods(bool $keys = false)
    {
        return $keys ? implode(',', array_keys($this->delivery_methods)) : $this->delivery_methods;
    }

    public function reportData(array $options)
    {
        return $this->post('/reports/reportData', array_merge([
            'uid' => '',
            'currentpage' => 0,
            'itemsPerPage' => '',
            'searchFields' => null,
        ], $options));
    }

    public function reports(bool $onDemandOnly = false)
    {
        return $this->post('/reports', [
            'custID' => $this->getCustomer(),
            'onDemandOnly' => $onDemandOnly
        ]);
    }

    public function reportDeliveryEndpoints($report)
    {
        if (!$report) {
            return null;
        }

        $endpointsData = $this->post('/reports/reportDeliveryEndpoints', [
            'reportUid' => $report,
            'deliveryMethod' => 'Sftp',
        ]);

        $endpoints = $endpointsData->endpoints;

        return collect($endpoints)->sortBy('name')->keyBy('_uid')->toArray();
    }

    public function schedules(array $options)
    {
        return $this->post('/reports/schedules', array_merge([
            'custID' => $this->getCustomer(),
            'reportUid' => '',
            'scheduleUid' => '',
            'currentpage' => 0,
            'itemsPerPage' => '',
            'frequency' => '',
            'startDateTime' => '',
            'endDateTime' => '',
        ], $options));
    }

    public function addOrUpdateSchedule(array $data)
    {
        $startDateTime = '';
        $endDateTime = '';

        session(['timezone_offset' => $data['utc_offset']]);

        if ($data['starts']) {
            $startDateTime = $data['starts'] . ' ' . $data['starts-time'];
            $startDateTime = $this->getUtcTime($startDateTime);
        }

        if ($data['ends'] and $data['frequency']) {
            $endDateTime = $data['ends'] . ' ' . $data['ends-time'];
            $endDateTime = $this->getUtcTime($endDateTime);
        }

        if ($data['delivery_method']) {
            $deliveryData = $data['endpoint'];
        } else {
            $deliveryData = $data['emails'];
        }

        Log::info("Post to /reports/addOrUpdateSchedule");
        Log::info("_uid: " . $data['schedule_uid']);
        Log::info("_reportUid: " . $data['report']);
        Log::info("startDateTime: " . $startDateTime);
        Log::info("frequency: " . $data['frequency']);
        Log::info("endDateTime: " . $endDateTime);
        Log::info("deliveryMethod: " . $data['delivery_method']);
        Log::info("deliveryData: " . $deliveryData);
        Log::info("status: " . $this->getStatuses()[0]); 

        return $this->post('/reports/addOrUpdateSchedule', [
            'schedule' => [
                '_uid' => $data['schedule_uid'] ?? '',
                '_reportUid' => $data['report'],
                'name' => 'Add Or Update Schedule',
                'scheduleDetails' => [
                    'startDateTime' => $startDateTime,
                    'frequency' => $data['frequency'],
                    'endDateTime' => $endDateTime,
                ],
                'deliveryMethod' => $data['delivery_method'],
                'deliveryData' => $deliveryData,
                'status' => $this->getStatuses()[0],
            ],
        ]);
    }

    private function getUtcTime($datetime)
    {
        $timezone_offset = session()->has('timezone_offset') ? session('timezone_offset') : 0;
        $minutes_to_add = $timezone_offset;
        $time = new DateTime($datetime);
        $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
        $stamp = $time->format('Y-m-d\TH:i:s.z');
        $utc_time = gmdate('Y-m-d\TH:i:s.z', strtotime($stamp)) . 'Z';
        return $utc_time;
    }

    public function getStatuses(): array
    {
        return $this->statuses;
    }

    public function updateScheduleStatus(array $data, $schedule)
    {
        return $this->post('/reports/addOrUpdateSchedule', [
            'schedule' => [
                '_uid' => $data['uid'],
                '_reportUid' => $schedule->_reportUid,
                'scheduleDetails' => [
                    'startDateTime' => $schedule->scheduleDetails->startDateTime,
                    'frequency' => $schedule->scheduleDetails->frequency,
                    'endDateTime' => $schedule->scheduleDetails->endDateTime,
                ],
                'deliveryMethod' => $schedule->deliveryMethod,
                'deliveryData' => $schedule->deliveryData,
                'status' => $data['status'],
            ],
        ]);
    }
}