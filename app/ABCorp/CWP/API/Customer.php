<?php


namespace App\ABCorp\CWP\API;


class Customer extends CustomerWebPortal
{
    public function __construct()
    {
        parent::__construct();
    }

    public function customers()
    {
        return $this->post('/customers/query', [
            'customers' => []
        ]);
    }

    public function customerTheme(string $customer)
    {
        return $this->post('/customerTheme', [
            'custID' => $customer,
        ]);
    }
}