<?php


namespace App\ABCorp\CWP\API;


use Auth0\Login\Facade\Auth0;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;

class CustomerWebPortal
{
    /**
     * Current customer name.
     *
     * @var string
     */
    private $customer = '';

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('cwp.api.domain'),
            'headers' => [
                'Authorization' => 'Bearer ' . Auth0::getIdToken(),
                'Content-Type' => 'application/json',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getCustomer(): string
    {
        if (session()->has('customerName')) {
            $this->customer = session('customerName');
        } elseif (Cookie::get('customerName')) {
            $this->customer = Cookie::get('customerName');
        } else {
            $customer = new Customer();

            $result = $customer->customers();

            Cache::forever('customers', $result);

            if (!isset($result->customers) || !count((array)$result->customers)) {
                dd('Invalid customer');
            }

            foreach ((array)$result->customers as $cust) {
                if (isset($cust->dataPrepID)) {
                    $this->customer = $cust->dataPrepID;
                    break;
                }
            }

            $customerData = $customer->customerTheme($this->customer);

            session([
                'customerName' => $this->customer,
                'customerLogo' => $customerData->logo,
            ]);

            cookie('customerName', $this->customer);
            cookie('customerLogo', $customerData->logo);

            if (session()->has('timezone_offset')) {
                $this->timezone_offset = session('timezone_offset');
            } else {
                $this->timezone_offset = 0;
            }
        }

        return $this->customer;
    }

    /**
     * Make get API request.
     *
     * @param string $path
     * @param array $options
     * @return mixed
     */
    protected function get(string $path, array $options = [])
    {
        try {
            $response = $this->client->get(config('cwp.api.endpoint') . $path, ['query' => $options]);
        } catch (\Exception $exception) {
            return (string)$exception->getResponse()->getBody();
        }

        return $this->getResponse($response);
    }

    /**
     * Make post API request.
     *
     * @param string $path
     * @param array $options
     * @param bool $is_array
     * @return mixed
     */
    protected function post(string $path, array $options, bool $is_array = false)
    {
        try {
            $response = $this->client->post(config('cwp.api.endpoint') . $path, [
                'body' => json_encode($is_array ? [$options] : $options)
            ]);
        } catch (\Exception  $exception) {
            return (string)$exception->getResponse()->getBody();
        }

        return $this->getResponse($response);
    }

    /**
     * Returns prepared API response.
     *
     * @param $response
     * @return mixed
     */
    protected function getResponse($response)
    {
        try {
            return json_decode($response->getBody());
        } catch (\Exception $exception) {
            return (string)$exception->getMessage();
        }
    }
}