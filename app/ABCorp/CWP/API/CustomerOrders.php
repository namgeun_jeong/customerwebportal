<?php


namespace App\ABCorp\CWP\API;


class CustomerOrders extends CustomerWebPortal
{
    public function __construct()
    {
        parent::__construct();
    }

    public function customerOrders(array $options)
    {
        return $this->post('/customerOrders', array_merge([
            'customers' => [$this->getCustomer()],
            'currentpage' => 0,
            'itemsPerPage' => '',
            'startDateTime' => '',
            'endDateTime' => '',
        ], $options));
    }

    public function rejectedItems(array $options)
    {
        return $this->post('/customerOrders/rejectedItems', array_merge([
            'custOrderUid' => '',
            'currentpage' => 0,
            'itemsPerPage' => '',
            'startDateTime' => '',
            'endDateTime' => '',
        ], $options));
    }
}