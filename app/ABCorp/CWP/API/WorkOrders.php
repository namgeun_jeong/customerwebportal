<?php


namespace App\ABCorp\CWP\API;


class WorkOrders extends CustomerWebPortal
{
    public function __construct()
    {
        parent::__construct();
    }

    public function workOrders(array $options)
    {
        return $this->post('/workOrders', array_merge([
            'custOrderUids'    => [],
            'selectedStatuses' => [],
            'selectedProducts' => [],
            'customers'        => [$this->getCustomer()],
            'currentpage'      => 0,
            'itemsPerPage'     => '',
            'startDateTime'    => '',
            'endDateTime'      => '',
        ], $options));
    }

    public function details(string $uid)
    {
        return $this->post('/workOrders/details', [
            'workOrderUid' => $uid,
        ]);
    }
}