<?php


namespace App\ABCorp\CWP\API;

use Illuminate\Support\Facades\Cache;

class OrderItems extends CustomerWebPortal
{
    /**
     * Product items pull actions.
     *
     * @var array
     */
    private $actions = [
        'destroy' => [
            'title' => 'Pull & Destroy',
            'code' => 'pullAndPurge',
            'tab' => 'Destroy',
        ],
        'reroute' => [
            'title' => 'Pull & Forward',
            'code' => 'pullAndForward',
            'tab' => 'Reroute',
        ],
        'prioritize' => [
            'title' => 'Pull & Rush',
            'code' => 'pullAndRush',
            'tab' => 'Prioritize',
        ],
    ];

    /**
     * Countries list for pull requests.
     *
     * @var array
     */
    private $countries = [
        'US' => 'United States',
        'AU' => 'Australia',
        'NZ' => 'New Zealand',
        'CN' => 'China',
        'RU' => 'Russian Federation',
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function getCountries(): array
    {
        $countries = $this->countries;

        return Cache::rememberForever('countries', function () use ($countries) {
            return $countries;
        });
    }

    public function getItemsActions($data)
    {
        if (empty($data->data)) {
            return $data;
        }

        foreach ($data->data as $key => $item) {
            $actions = [];

            foreach ($this->getActions() as $tab => $action) {
                if ($item->{'#' . $action['code']}) {
                    $actions[] = $tab;
                }
            }

            $data->data[$key]->actions = json_encode($actions);
        }

        return $data;
    }

    public function getActions(bool $keys = false)
    {
        return $keys ? implode(',', array_keys($this->actions)) : $this->actions;
    }

    public function orderItems(array $options)
    {
        return $this->post('/orderItems', array_merge([
            'workOrderUid' => '',
            'currentpage' => 0,
            'itemsPerPage' => '',
            'searchFields' => null,
            'startDateTime' => '',
            'endDateTime' => '',
        ], $options));
    }

    public function itemDetails(string $uid)
    {
        return $this->post('/orderItems/itemDetails', [
            'orderItemUid' => $uid,
        ]);
    }

    public function byProduct(array $options = [])
    {
        return $this->post('/orderItems/byProduct', array_merge([
            'custID' => $this->getCustomer(),
            'prodVerticalUid' => '',
            'prodID' => null,
            'currentpage' => 0,
            'itemsPerPage' => '',
            'searchFields' => null,
            'startDateTime' => '',
            'endDateTime' => '',
        ], $options));
    }

    public function pullAction(array $data)
    {
        $actionCode = array_search(
            $data['action'],
            array_keys($this->getActions())
        );

        if (isset($data['prioritize'])) {
            $actionCode = 1;
            $data['rushForwardRequested'] = true;
            $data['delivery_date'] = \Carbon\Carbon::parse($data['delivery_date'])->toDateTimeString();
        }

        try {
            $params = [
                'orderItemUid' => $data['uid'],
                'actionCode' => $actionCode,
                'remark' => '',
                'actionData' => [
                    'shippingDetail' => isset($data['shippingDetail'])
                        ? $data['shippingDetail'] : null,
                ],
            ];

            if (isset($data['rushForwardRequested'])) {
                $params['actionData']['rushForwardRequested'] = $data['rushForwardRequested'];
            }

            if (isset($data['delivery_date'])) {
                $params['actionData']['expectedDateTime'] = $data['delivery_date'];
            }

            return $this->post('/orderItems/pullAction', $params, true);
        } catch (\Exception $e) {
            return (string)$e->getResponse()->getBody();
        }
    }
}