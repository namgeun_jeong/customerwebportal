<?php


namespace App\ABCorp\CWP\API;


class CustomerRequests extends CustomerWebPortal
{
    private $statuses = [
        'Pending',
        'InProgress',
        'Rejected',
        'Closed',
        'Cancelled',
    ];

    private $carriers = [
        'UPS',
        'FedEx'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function getCarriers(): array
    {
        return $this->carriers;
    }

    public function getStatuses(): array
    {
        return $this->statuses;
    }

    public function itemActions(array $options)
    {
        return $this->post('/customerRequests/itemActions', array_merge([
            'customers' => [$this->getCustomer()],
            'statuses' => [],
            'actions' => [],
            'currentpage' => 0,
            'itemsPerPage' => '',
            'startDateTime' => '',
            'endDateTime' => '',
        ], $options));
    }

    public function itemActionDetails($uid)
    {
        return $this->post('/customerRequests/itemActionDetails', [
            'itemActioRequestUid' => $uid,
            'currentpage' => 0,
            'itemsPerPage' => 25,
        ]);
    }

    public function updateItemActionStatus(array $options)
    {
        return $this->post('/customerRequests/updateItemActionStatus', array_merge([
            'itemActioRequestUid' => '',
            'actionData' => '',
            'status' => '',
            'carrier' => '',
            'trackingNo' => '',
        ], $options));
    }
}