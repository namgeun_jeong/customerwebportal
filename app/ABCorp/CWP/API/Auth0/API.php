<?php

namespace App\ABCorp\CWP\API\Auth0;

use Illuminate\Support\Facades\Log;

abstract class API
{
    protected $client;

    protected $endpoint;

    public function get(string $path, array $options = [])
    {
        return $this->request($path, 'GET', ['query' => $options]);
    }

    public function post(string $path, array $options)
    {
        return $this->request($path, 'POST', ['body' => json_encode($options)]);
    }

    public function patch(string $path, array $options)
    {
        return $this->request($path, 'PATCH', ['body' => json_encode($options)]);
    }

    public function delete(string $path, array $options = [])
    {
        return $this->request($path, 'DELETE', ['body' => json_encode($options)]);
    }

    protected function request(string $path, string $method = 'GET', array $options = [])
    {
        try {
            $response = $this->client->request($method, $this->endpoint . $path, $options);
        } catch (\Throwable $throwable) {
            Log::error($throwable->getMessage());

            dd('Unexpected error');
        }

        return $this->response($response);
    }

    /**
     * Returns prepared API response.
     *
     * @param $response
     * @return mixed
     */
    protected function response($response)
    {
        try {
            return json_decode($response->getBody());
        } catch (\Exception $e) {
            return (string)$e->getMessage();
        }
    }
}