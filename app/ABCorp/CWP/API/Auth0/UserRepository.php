<?php

namespace App\ABCorp\CWP\API\Auth0;

use App\Http\Requests\StoreUser;
use App\Traits\Configuration;
use App\Traits\Users;

class UserRepository implements UserRepositoryInterface
{
    use Users, Configuration;

    private $auth_ext;

    private $groupsRepository;

    private $rolesRepository;

    private $auth;

    public function __construct()
    {
        $this->auth = new Auth0API();
        $this->auth_ext = new AuthExtension();
        $this->groupsRepository = new GroupsRepository();
        $this->rolesRepository = new RolesRepository();
    }

    /**
     * Store user.
     *
     * @param StoreUser $request
     * @return bool|mixed
     */
    public function storeUser(StoreUser $request)
    {
        $currentGroup = $this->groupsRepository->getGroup();
        $currentGroupId = $this->groupsRepository->getGroupIdByTitle($currentGroup);

        if (!$currentGroupId) {
            dd('Invalid group!');
        }

        $user = $this->getUserByEmail($request->email);

        if (!$this->valid($user)) {
            $user = $this->store(['email' => $request->email]);
        }

        return $this->addGroups($user, [$currentGroupId]);
    }

    /**
     * Get user by email.
     *
     * @param string $email
     * @return array|mixed
     */
    private function getUserByEmail(string $email)
    {
        $result = $this->auth->get('/v2/users-by-email', ['email' => $email]);

        return empty($result) ? false : $result[0];
    }

    /**
     * Store new user request.
     *
     * @param array $options
     * @return mixed
     */
    private function store(array $options)
    {
        return $this->auth->post('/v2/users', array_merge([
            'connection' => config('laravel-auth0.connection'),
            'email' => $options['email'],
            'password' => 'secret',
            'app_metadata' => [
                'authorization' => [
                    'groups' => [],
                    'roles' => [],
                    'permissions' => [],
                ],
            ],

        ], $options));
    }

    /**
     * Add groups to user.
     *
     * @param $user
     * @param array $groups
     * @return mixed
     */
    private function addGroups($user, array $groups)
    {
        $this->auth_ext->patch("/users/{$user->user_id}/groups", $groups);

        $groups = $this->getUserGroups($user->user_id);

        return $this->updateMetadata($user, [
            'groups' => array_values($groups),
        ]);
    }

    /**
     * Get user groups.
     *
     * @param string $user_id
     * @return array
     */
    private function getUserGroups(string $user_id): array
    {
        $result = $this->auth_ext->get("/users/{$user_id}/groups");

        $groups = [];

        foreach ($result as $group) {
            $groups[$group->_id] = $group->name;
        }

        return $groups;
    }

    /**
     * Update user metadata.
     *
     * @param $user
     * @param array $metadata
     * @return mixed
     */
    private function updateMetadata($user, array $metadata)
    {
        $app_metadata = [
            'app_metadata' => [
                'authorization' => [
                    'groups' => isset($metadata['groups']) ? $metadata['groups'] : $user->app_metadata->authorization->groups,
                    'permissions' => isset($metadata['permissions']) ? $metadata['permissions'] : $user->app_metadata->authorization->permissions,
                    'roles' => isset($metadata['roles']) ? $metadata['roles'] : $user->app_metadata->authorization->roles,
                ],
            ],
        ];

        return $this->updateUser($user->user_id, $app_metadata);
    }

    /**
     * Update user request.
     *
     * @param string $user_id
     * @param $data
     * @return mixed
     */
    private function updateUser(string $user_id, $data)
    {
        return $this->auth->patch('/v2/users/' . $user_id, $data);
    }

    /**
     * Get user request.
     *
     * @param string $user_id
     * @return mixed
     */
    public function getUser(string $user_id)
    {
        return $this->auth->get('/v2/users/' . $user_id);
    }

    /**
     * Update user role.
     *
     * @param $user
     * @param array $roles
     * @return mixed
     */
    public function updateRoles($user, array $roles)
    {
        $this->deleteUserRoles($user->user_id);

        return $this->addRoles($user, $roles);
    }

    /**
     * Delete user from all roles.
     *
     * @param string $user_id
     */
    private function deleteUserRoles(string $user_id)
    {
        $user_roles = $this->getUserRoles($user_id);

        if (count($user_roles)) {
            $this->auth_ext->delete("/users/$user_id/roles", array_keys($user_roles));
        }
    }

    /**
     * Get user roles.
     *
     * @param string $user_id
     * @return array
     */
    public function getUserRoles(string $user_id): array
    {
        $result = $this->auth_ext->get("/users/{$user_id}/roles");

        $roles = [];

        foreach ($result as $role) {
            $roles[$role->_id] = $role->name;
        }

        return $roles;
    }

    /**
     * Add roles to user.
     *
     * @param $user
     * @param array $roles
     * @return mixed
     */
    private function addRoles($user, array $roles)
    {
        $this->auth_ext->patch("/users/{$user->user_id}/roles", $roles);

        $roles = $this->getUserRoles($user->user_id);

        return $this->updateMetadata($user, [
            'roles' => array_values($roles),
        ]);
    }

    /**
     * Change password request.
     *
     * @param $data
     * @return mixed
     */
    public function changePassword($data)
    {
        return $this->auth->changePassword($data);
    }

    /**
     * Delete user request.
     *
     * @param $user_id
     * @return mixed
     */
    public function destroy(string $user_id)
    {
        $this->deleteUserRoles($user_id);
        $this->deleteUserGroups($user_id);

        return $this->auth->delete('/v2/users/' . $user_id);
    }

    /**
     * Delete user from all groups.
     *
     * @param string $user_id
     */
    private function deleteUserGroups(string $user_id)
    {
        $user_groups = $this->getUserGroups($user_id);

        if (count($user_groups)) {
            foreach ($user_groups as $group_id => $group_title) {
                $this->auth_ext->delete("/groups/$group_id/members", [$user_id]);
            }
        }
    }
}