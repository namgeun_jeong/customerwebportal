<?php

namespace App\ABCorp\CWP\API\Auth0;

use Illuminate\Support\Facades\Cache;

class RolesRepository implements RolesRepositoryInterface
{
    private $auth_ext;

    private $roles;

    public function __construct()
    {
        $this->auth_ext = new AuthExtension();

        $this->roles = Cache::remember('roles', 60, function () {
            return $this->get();
        });
    }

    /**
     * Get a list of roles.
     *
     * @return array
     */
    private function get()
    {
        $result = $this->auth_ext->get('/roles');

        $roles = [];

        foreach ($result->roles as $role) {
            $roles[$role->_id] = $role->name;
        }

        return $roles;
    }

    /**
     * Roles getter.
     *
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }
}