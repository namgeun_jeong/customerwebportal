<?php

namespace App\ABCorp\CWP\API\Auth0;

use GuzzleHttp\Client;

class AuthExtension extends API
{
    public function __construct()
    {
        $client = new Client();

        $response = $client->post('https://' . config('laravel-auth0.domain') . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => config('laravel-auth0.client_id'),
                'client_secret' => config('laravel-auth0.client_secret'),
                'audience' => 'urn:auth0-authz-api',
            ],
        ]);

        $result = json_decode($response->getBody());

        $this->client = new Client([
            'base_uri' => config('cwp.api.configuration_domain'),
            'headers' => [
                'Authorization' => 'Bearer ' . $result->access_token,
                'Content-Type' => 'application/json',
            ],
        ]);

        $this->endpoint = config('cwp.api.configuration_endpoint');
    }
}