<?php

namespace App\ABCorp\CWP\API\Auth0;

use GuzzleHttp\Client;

class Auth0API extends API
{
    public function __construct()
    {
        $client = new Client();

        $response = $client->post('https://' . config('laravel-auth0.domain') . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => config('laravel-auth0.client_id'),
                'client_secret' => config('laravel-auth0.client_secret'),
                'audience' => 'https://' . config('laravel-auth0.domain') . '/api/v2/',
            ],
        ]);

        $result = json_decode($response->getBody());

        $this->client = new Client([
            'base_uri' => 'https://' . config('laravel-auth0.domain'),
            'headers' => [
                'Authorization' => 'Bearer ' . $result->access_token,
                'Content-Type' => 'application/json',
            ],
        ]);

        $this->endpoint = '/api';
    }

    public function changePassword($data)
    {
        $this->endpoint = '';

        return $this->post('/dbconnections/change_password', $data);
    }
}