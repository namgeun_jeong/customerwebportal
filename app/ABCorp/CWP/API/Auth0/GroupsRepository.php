<?php

namespace App\ABCorp\CWP\API\Auth0;

use Illuminate\Support\Facades\Cache;

class GroupsRepository implements GroupsRepositoryInterface
{
    private $auth_ext;

    private $groups;

    public function __construct()
    {
        $this->auth_ext = new AuthExtension();

        $this->groups = Cache::remember('groups', 60, function () {
            return $this->get();
        });
    }

    /**
     * Get a list of groups(customers).
     *
     * @return array
     */
    private function get()
    {
        $result = $this->auth_ext->get('/groups');

        $groups = [];

        foreach ($result->groups as $group) {
            $groups[$group->_id] = $group->name;
        }

        return $groups;
    }

    /**
     * Get members for a given group.
     *
     * @param string $group_id
     * @return array
     */
    public function getGroupMembers(string $group_id = ''): array
    {
        if (!$group_id) {
            $group_id = $this->getGroupIdByTitle();
        }

        $result = $this->auth_ext->get("/groups/$group_id/members");

        $users = isset($result->users) ? $result->users : [];

        return array_where($users, function ($value, $key) {
            return isset($value->created_at);
        });
    }

    /**
     * Get group id by title.
     *
     * @param string $title
     * @return string
     */
    public function getGroupIdByTitle(string $title = ''): string
    {
        if (!$title) {
            $title = $this->getGroup();
        }

        return array_search($title, $this->getGroups());
    }

    /**
     * Get current customer group.
     *
     * @return string
     */
    public function getGroup()
    {
        return session('customerName');
    }

    /**
     * Groups list getter.
     *
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }
}