<?php


namespace App\ABCorp\CWP\API;


class Product extends CustomerWebPortal
{
    public function __construct()
    {
        parent::__construct();
    }

    public function byCustomer()
    {
        return $this->get('/products/byCustomer/' . $this->getCustomer());
    }

    public function addOrUpdateProductVertical(array $options = [])
    {

        return $this->post('/products/addOrUpdateProductVertical', array_merge([
            'customer' => $this->getCustomer(),
            'productVertical' => [
                'uid' => '',
                'name' => '',
                'desc' => '',
                'searchFields' => [],
                'displayFields' => [],
            ]
        ], $options));
    }

    public function addOrUpdateProductWithPart(array $options = [])
    {
        return $this->post('/products/addOrUpdateProductWithPart', array_merge([
            'prodVerticalUid' => '',
            'productWithPart' => [
                'part' => [
                    'uid' => '',
                    'name' => '',
                    'coID' => '',
                    'partID' => '',
                    'unitPrice' => 0,
                    'jobCopy' => '',
                    'engID' => '',
                    'engLotID' => '',
                ],
                'uid' => '',
                'name' => '',
                'dataPrepID' => '',
            ]
        ], $options));
    }
}