<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductItem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prodVerticalUid' => ['required'],
            'productWithPart.uid' => ['required'],
            'productWithPart.name' => ['required'],
            'productWithPart.dataPrepID' => ['required'],
            'productWithPart.part.*' => ['required'],
        ];
    }
}
