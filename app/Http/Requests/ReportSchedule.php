<?php

namespace App\Http\Requests;

use App\ABCorp\CWP\API\Reports;
use Illuminate\Foundation\Http\FormRequest;

class ReportSchedule extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reports = new Reports();

        return [
            'report' => 'required',
            'starts' => 'required|date_format:"Y-m-d"',
            'starts-time' => 'required',
            'frequency' => 'required|in:' . $reports->getFrequencies(true),
            'ends' => 'nullable|date_format:"Y-m-d"|after:starts',
            'ends-time' => 'required_with:ends',
            'delivery_method' => 'required|in:' . $reports->getDeliveryMethods(true),
            'emails' => 'bail|required_if:delivery_method,0|emails:' . $this->delivery_method,
            'endpoint' => 'required_if:delivery_method,1'
        ];
    }

    public function messages()
    {
        return [
            'emails' => 'Emails field must have valid email addresses.',
            'emails.required_if' => 'Emails field is required',
            'endpoint.required_if' => 'Endpoint field is required',
        ];
    }
}
