<?php

namespace App\Http\Requests;

use App\ABCorp\CWP\API\OrderItems;
use Illuminate\Foundation\Http\FormRequest;

class PullActionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // @TODO who can make a request?
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $orderItems = new OrderItems();

        $rules = array_merge([
            'uid' => 'required',
            'action' => 'required|in:' . $orderItems->getActions(true),
        ], $this->getActionRules($this->action));

        return $rules;
    }

    private function getActionRules($action)
    {
        $rules = [];

        if ($action === 'reroute') {
            $orderItems = new OrderItems();

            $rules = [
                'shippingDetail.companyName' => '',
                'shippingDetail.attentionTo' => '',
                'shippingDetail.address1' => 'required',
                'shippingDetail.address2' => '',
                'shippingDetail.address3' => '',
                'shippingDetail.locality' => 'required',
                'shippingDetail.region' => 'required',
                'shippingDetail.postcode' => 'required',
                'shippingDetail.countryCode' => 'required|in:' . implode(',', array_keys($orderItems->getCountries())),
                'shippingDetail.email' => 'nullable|email',
                'shippingDetail.phone' => '',
            ];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'shippingDetail.address1.required' => 'The Address1 field is required.',
            'shippingDetail.locality.required' => 'The Locality field is required.',
            'shippingDetail.region.required' => 'The Region field is required.',
            'shippingDetail.postcode.required' => 'The Post Code field is required.',
            'shippingDetail.countryCode.required' => 'The Country Code field is required.',
            'shippingDetail.email.email' => 'The Email field must be a valid email address.',
        ];
    }
}
