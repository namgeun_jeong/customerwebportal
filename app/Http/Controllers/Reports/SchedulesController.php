<?php

namespace App\Http\Controllers\Reports;

use App\ABCorp\CWP\API\Reports;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReportSchedule;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SchedulesController extends Controller
{
    private $reports;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->reports = new Reports();

            return $next($request);
        });
    }

    /**
     * Display a listing of scheduled tasks for the report.
     *
     * @param string $report
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function data(string $report = '', Request $request)
    {
        $data = $this->reports->schedules([
            'reportUid' => $report,
            'currentpage' => $this->getCurrentPage($request),
        ]);

        $endpointsData = $this->reports->reportDeliveryEndpoints($report);

        return $this->response('pages.schedules', $request, [
            'data' => $data,
            'reports' => $this->reports->reports(),
            'report' => $report ?? null,
            'endpoints' => $endpointsData,
            'frequencies' => $this->reports->getFrequencies(),
            'delivery_methods' => $this->reports->getDeliveryMethods(),
        ]);
    }

    public function add($report = '')
    {
        $endpointsData = $this->reports->reportDeliveryEndpoints($report);

        return view('pages.schedules.form', [
            'report' => $report,
            'reports' => $this->reports->reports(),
            'frequencies' => $this->reports->getFrequencies(),
            'delivery_methods' => $this->reports->getDeliveryMethods(),
            'endpoints' => $endpointsData,
        ]);
    }

    public function edit(string $schedule = '')
    {
        $schedule = $this->reports->schedules([
            'scheduleUid' => $schedule,
        ]);

        // $timezone_offset = session()->has('timezone_offset') ? session('timezone_offset') : 0;

        // if ($schedule->data[0]->scheduleDetails->startDateTime) {
        //     $schedule->data[0]->scheduleDetails->startDateTime = Carbon::parse($schedule->data[0]->scheduleDetails->startDateTime)
        //         ->subMinutes($timezone_offset);
        // }

        // if ($schedule->data[0]->scheduleDetails->endDateTime) {
	    //     $schedule->data[0]->scheduleDetails->endDateTime = Carbon::parse($schedule->data[0]->scheduleDetails->endDateTime)
        //         ->subMinutes($timezone_offset);
        // }
        
        $report = $schedule->data[0]->_reportUid;
        $endpointsData = $this->reports->reportDeliveryEndpoints($report);

        return view('pages.schedules.form', [
            'report' => $report,
            'schedule' => $schedule->data[0],
            'endpoints' => $endpointsData,
            'frequencies' => $this->reports->getFrequencies(),
            'delivery_methods' => $this->reports->getDeliveryMethods(),
        ]);
    }

    /**
     * Add or update a schedule for a report task.
     *
     * @param ReportSchedule $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ReportSchedule $request)
    {
        $results = $this->reports->addOrUpdateSchedule($request->all());

        if (isset($results->_uid)) {
            return response()->json([
                'title' => __('Schedule added'),
                'message' => __('You has been successfully added a schedule for a report task.'),
            ]);
        }

        return response()->json(['error' => $results]);
    }

    // @TODO Check and fix this
    public function changeStatus(string $uid, string $status)
    {
        if (!in_array($status, $this->reports->getStatuses())) {
            dd('Wrong status!');
        }

        $schedule = $this->reports->schedules([
            'scheduleUid' => $uid,
        ]);

        $results = $this->reports->updateScheduleStatus([
            'uid' => $uid,
            'status' => $status,
        ], $schedule->data[0]);

        if (isset($results->_uid)) {
            return response()->json('The schedule was successfully updated.', 200);
        }

        return response()->json('Failed to update the schedule.', 422);
    }
}
