<?php

namespace App\Http\Controllers\Reports;

use App\ABCorp\CWP\API\Reports;
use App\Http\Controllers\Controller;
use App\Traits\AdvancedSearch;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    use AdvancedSearch;

    private $reports;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->reports = new Reports;

            return $next($request);
        });
    }

    /**
     * Get data for the report.
     *
     * @param string $report
     * @param string $name
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
     * @throws \Throwable
     */
    public function data(string $report, string $name = '', Request $request)
    {
        $_roles =  auth()->user()->getUserInfo();
        $_role = $_roles['roles'][0];

        $reports = $this->reports->reports(true,$_role);

        if (empty($reports->data)) {
            return redirect()->route('customers.data');
        }

        if ($report && $name) {
            $uids = [];

            foreach ($reports->data as $rep) {
                if ($rep->name === $name && $rep->uid !== $report) {
                    return redirect()->route('reports.data', ['report' => $rep->uid, 'name' => $rep->name]);
                }
                $uids[] = $rep->uid;
            }

            if (!in_array($report, $uids)) {
                return redirect()->route('reports.data',
                    ['report' => $reports->data[0]->uid, 'name' => $reports->data[0]->name]);
            }
        }

        $searchData = $this->reports->reportData([
            'uid' => $report,
            'currentpage' => 1,
            'itemsPerPage' => 0,
        ]);

        $data = $this->reports->reportData([
            'uid' => $report,
            'currentpage' => $this->getCurrentPage($request),
            'searchFields' => isset($searchData->searchFields) ? $this->getSearchFields($searchData->searchFields) : [],
        ]);

        if (is_string($data)) {
            return $data;
        }

        return $this->response('pages.reports', $request, [
            'data' => $data,
            'report' => $report,
            'include_search' => $this->isIncludeSearch($data->searchFields),
        ]);
    }

    public function getDeliveryMethod($report)
    {
        if ($report) {
            $endpoints = $this->reports->reportDeliveryEndpoints($report);

            return view('pages.schedules.delivery-method', compact('endpoints'));
        }
    }
}
