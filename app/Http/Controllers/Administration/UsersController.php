<?php

namespace App\Http\Controllers\Administration;

use App\ABCorp\CWP\API\Auth0\GroupsRepositoryInterface;
use App\ABCorp\CWP\API\Auth0\RolesRepositoryInterface;
use App\ABCorp\CWP\API\Auth0\UserRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUserRoles;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Get users that belong to the current customer.
     *
     * @param GroupsRepositoryInterface $groupsRepository
     * @return \Illuminate\View\View
     */
    public function index(GroupsRepositoryInterface $groupsRepository)
    {
        $members = $groupsRepository->getGroupMembers();

        return view('pages.administration.users.index', compact('members'));
    }

    /**
     * Show the form for creating a new user.
     *
     * @param RolesRepositoryInterface $rolesRepository
     * @return \Illuminate\Http\Response
     */
    public function create(RolesRepositoryInterface $rolesRepository)
    {
        return view('pages.administration.users.create')
            ->withRoles($rolesRepository->getRoles());
    }

    /**
     * Store a newly created user.
     *
     * @param StoreUser|Request $request
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request, UserRepositoryInterface $userRepository)
    {
        $user = $userRepository->storeUser($request);

        if (isset($user->user_id)) {
            return redirect(route('users.show', $user->user_id))->with([
                'message' => 'The user was successfully stored.',
                'alert-type' => 'success',
            ]);
        }

        return redirect(route('users.index'))->with([
            'message' => 'Failed to store the user.',
            'alert-type' => 'error',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param UserRepositoryInterface $userRepository
     * @param RolesRepositoryInterface $rolesRepository
     * @return \Illuminate\Http\Response
     */
    public function show($id, UserRepositoryInterface $userRepository, RolesRepositoryInterface $rolesRepository)
    {
        $user = $userRepository->getUser($id);

        if (!$userRepository->valid($user)) {
            return redirect(route('users.index'));
        }

        $roles = $rolesRepository->getRoles();

        $user_roles = $userRepository->getUserRoles($user->user_id);

        return view('pages.administration.users.show', compact('user', 'roles', 'user_roles'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, UserRepositoryInterface $userRepository)
    {
        $result = $userRepository->destroy($id);

        if (!$result) {
            return redirect(route('users.index'))->with([
                'message' => 'The user was successfully deleted.',
                'alert-type' => 'success',
            ]);
        }

        $error = json_decode($result);

        return redirect(route('users.index'))->with([
            'message' => $error->message,
            'alert-type' => 'error',
        ]);
    }

    /**
     * Update user roles.
     *
     * @param UpdateUserRoles $request
     * @param string $id
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateRoles(UpdateUserRoles $request, string $id, UserRepositoryInterface $userRepository)
    {
        $user = $userRepository->getUser($id);

        if (!$userRepository->valid($user)) {
            return response()->json(['error' => $user, 'title' => 'Error']);
        }

        $user = $userRepository->updateRoles($user, $request->roles);

        if (isset($user->user_id)) {
            return response()->json(['message' => 'User roles has been successfully updated.']);
        }

        return response()->json(['error' => 'Failed to update user roles.']);
    }

    /**
     * Sent reset password link.
     *
     * @param string $id
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function passwordReset(string $id, UserRepositoryInterface $userRepository)
    {
        $user = $userRepository->getUser($id);

        $result = $userRepository->changePassword([
            "client_id" => config('laravel-auth0.client_id'),
            "email" => $user->email,
            "connection" => "Username-Password-Authentication",
        ]);

        if ($result === null) {
            return response()->json([
                'title' => __('Password reset'),
                'message' => __('Email with password reset link was sent to the user'),
            ]);
        } else {
            return response()->json(['error' => $result, 'title' => 'Something goes wrong. Please try again later.']);
        }
    }

    public function updateTimezone(Request $request)
    {
        session()->put('timezone_offset', $request->timezone_offset);
    }
}
