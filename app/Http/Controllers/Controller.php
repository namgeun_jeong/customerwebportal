<?php

namespace App\Http\Controllers;

use App\Traits\AdvancedSearch;
use App\Traits\Pagination;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Pagination, AdvancedSearch;

    /**
     * Prepare response.
     *
     * @param string $view
     * @param Request $request
     * @param array $options
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function response(string $view, Request $request, array $options)
    {
        $pagination = isset($options['data']->paging) ? $this->makePagination($options['data'], $request) : null;

        $options = array_merge($options, ['pagination' => $pagination]);

        if (request()->ajax()) {
            return view($view . '.table', $options)->render();
        }

        return view($view . '.index', $options);
    }
}
