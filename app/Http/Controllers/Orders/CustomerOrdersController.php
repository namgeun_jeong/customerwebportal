<?php

namespace App\Http\Controllers\Orders;

use App\ABCorp\CWP\API\CustomerOrders;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomerOrdersController extends Controller
{
    private $customerOrders;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->customerOrders = new CustomerOrders;

            return $next($request);
        });
    }

    /**
     * Get customer orders.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function data(Request $request)
    {
        $data = $this->customerOrders->customerOrders([
            'currentpage' => $this->getCurrentPage($request),
            'startDateTime' => $this->getStartDate($request, true),
            'endDateTime' => $this->getEndDate($request, true),
        ]);

        return $this->response('pages.orders.customers', $request, [
            'data' => $data,
            'date_from' => $this->getStartDate($request),
            'date_to' => $this->getEndDate($request),
        ]);
    }
}