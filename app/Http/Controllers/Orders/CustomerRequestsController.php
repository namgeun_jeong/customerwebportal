<?php

namespace App\Http\Controllers\Orders;

use App\ABCorp\CWP\API\CustomerRequests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateCustomerRequest;
use Illuminate\Http\Request;

class CustomerRequestsController extends Controller
{
    private $customerRequests;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->customerRequests = new CustomerRequests;

            return $next($request);
        });
    }

    /**
     * Get customers requests.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function data(Request $request)
    {
        $data = $this->customerRequests->itemActions([
            'currentpage' => $this->getCurrentPage($request),
            'statuses' => isset($request->statuses) ? $request->statuses : [],
            'actions' => isset($request->actions) ? $request->actions : [],
            'startDateTime' => $this->getStartDate($request, true),
            'endDateTime' => $this->getEndDate($request, true),
        ]);

        return $this->response('pages.orders.requests', $request, [
            'data' => $data,
            'date_from' => $this->getStartDate($request),
            'date_to' => $this->getEndDate($request),
        ]);
    }

    /**
     * Cancel customer request.
     *
     * @param string $uid
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(string $uid)
    {
        $results = $this->customerRequests->updateItemActionStatus([
            'itemActioRequestUid' => $uid,
            'status' => 'Cancelled',
        ]);

        if (isset($results->success) && $results->success) {
            return response()->json([
                'title' => __('cwp.requests.cancel.title'),
                'message' => __('cwp.requests.cancel.message'),
            ]);
        }

        return response()->json(['error' => $results, 'title' => 'Error']);
    }

    /**
     * Get details for the customer request.
     *
     * @param string $uid
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function details(string $uid)
    {
        $data = $this->customerRequests->itemActionDetails($uid);

        return view('pages.orders.requests.details', compact('data'))->render();
    }

    public function show(string $uid)
    {
        $data = $this->customerRequests->itemActionDetails($uid);

        $requestDetails = json_decode($data->details->requestDetails);

        $shippingDetail = (array)$requestDetails->shippingDetail;

        $rushForwardRequested = $requestDetails->rushForwardRequested;

        return view('pages.orders.requests.show', compact('data', 'shippingDetail', 'rushForwardRequested'));
    }

    /**
     * Get view for update modal.
     *
     * @param string $request
     *
     * @return string
     * @throws \Throwable
     */
    public function edit(string $request)
    {
        $data = $this->customerRequests->itemActionDetails($request);

        $statuses = $this->customerRequests->getStatuses();

        $carriers = $this->customerRequests->getCarriers();

        return view('pages.orders.requests.update-form', compact('data', 'statuses', 'carriers'))->render();
    }

    /**
     * Update customer request.
     *
     * @param string $request_uid
     * @param UpdateCustomerRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(string $request_uid, UpdateCustomerRequest $request)
    {
        $results = $this->customerRequests->updateItemActionStatus([
            'itemActioRequestUid' => $request_uid,
            'actionData' => $request->message ? $request->message : '',
            'status' => $request->status,
            'carrier' => $request->carrier,
            'trackingNo' => $request->trackingNo,
        ]);

        if (isset($results->success) && $results->success) {
            return response()->json([
                'title' => __('cwp.requests.update.title'),
                'message' => __('cwp.requests.update.message'),
            ]);
        }

        return response()->json(['error' => $results, 'title' => 'Error']);
    }
}
