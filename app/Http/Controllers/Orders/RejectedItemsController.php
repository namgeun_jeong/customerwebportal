<?php

namespace App\Http\Controllers\Orders;

use App\ABCorp\CWP\API\CustomerOrders;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RejectedItemsController extends Controller
{
    private $customerOrdersRepository;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->customerOrdersRepository = new CustomerOrders;

            return $next($request);
        });
    }

    /**
     * Get rejected items list for the customer order.
     *
     * @param string $customer_order_uid
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function data(string $customer_order_uid, Request $request)
    {
        $data = $this->customerOrdersRepository->rejectedItems([
            'custOrderUid' => $customer_order_uid,
            'currentpage' => $this->getCurrentPage($request),
        ]);

        return $this->response('pages.orders.rejected', $request, [
            'data' => $data,
        ]);
    }
}
