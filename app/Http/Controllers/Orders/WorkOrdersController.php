<?php

namespace App\Http\Controllers\Orders;

use App\ABCorp\CWP\API\WorkOrders;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WorkOrdersController extends Controller
{
    private $workOrders;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->workOrders = new WorkOrders;

            return $next($request);
        });
    }

    /**
     * Get work orders.
     *
     * @param string $customer_order_uid
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function data(string $customer_order_uid = '', Request $request)
    {
        $data = $this->workOrders->workOrders([
            'currentpage' => $this->getCurrentPage($request),
            'custOrderUids' => $customer_order_uid ? [$customer_order_uid] : [],
            'startDateTime' => $this->getStartDate($request, true),
            'endDateTime' => $this->getEndDate($request, true),
            'selectedStatuses' => isset($request->statuses) ? $request->statuses : [],
            'selectedProducts' => isset($request->products) ? $request->products : [],
        ]);

        if (!isset($data->data)) {
            return redirect('/');
        }

        return $this->response('pages.orders.work', $request, [
            'data' => $data,
            'date_from' => $this->getStartDate($request),
            'date_to' => $this->getEndDate($request),
            'customer_order_uid' => $customer_order_uid,
        ]);
    }

    /**
     * Get details for the work order.
     *
     * @param string $work_order_uid
     *
     * @return \Illuminate\View\View
     */
    public function show(string $work_order_uid)
    {
        $details = $this->workOrders->details($work_order_uid);

        if (!isset($details->data)) {
            return response()->json(['error' => $details, 'title' => 'Error'], 400);
        }

        return view('pages.orders.work.show', compact('details'));
    }
}
