<?php

namespace App\Http\Controllers\Orders;

use App\ABCorp\CWP\API\OrderItems;
use App\ABCorp\CWP\API\Product;
use App\Http\Controllers\Controller;
use App\Traits\AdvancedSearch;
use Illuminate\Http\Request;

class ProductItemsController extends Controller
{
    use AdvancedSearch;

    private $orderItems;

    private $product;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orderItems = new OrderItems;
            $this->product = new Product;

            return $next($request);
        });
    }

    /**
     * Get items list.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function data(Request $request)
    {
        $prodID = isset($request->products) ? $request->products : '';
        $vertical = isset($request->vertical) ? $request->vertical : '';

        $searchData = $this->orderItems->byProduct([
            'currentpage' => 1,
            'itemsPerPage' => 0,
            'prodID' => $prodID,
            'prodVerticalUid' => $vertical
        ]);

        if (!isset($searchData->data) && !request()->ajax()) {
            return view('pages.orders.products.index');
        }

        $data = $this->orderItems->byProduct([
            'prodVerticalUid' => $vertical,
            'currentpage' => $this->getCurrentPage($request),
            'prodID' => $prodID,
            'searchFields' => isset($searchData->searchFields) ? $this->getSearchFields($searchData->searchFields) : null,
        ]);

        $data = $this->orderItems->getItemsActions($data);

        if (isset($data->productVerticals)) {
            $verticals = [];

            foreach ($data->productVerticals as $vertical) {
                $products = [];

                foreach ($data->products as $product) {
                    if (in_array($product->key, $vertical->value->prodList)) {
                        $products[$product->key] = $product->value;
                    }
                }

                $verticals[$vertical->key] = [
                    'name' => $vertical->value->name,
                    'products' => $products
                ];
            }

            return $this->response('pages.orders.products', $request, [
                'data' => $data,
                'selectedVertical' => $data->selectProductVertical,
                'verticals' => $verticals,
                'include_search' => $this->isIncludeSearch($data->searchFields),
                'countries' => $this->orderItems->getCountries(),
            ]);
        }
    }
}