<?php

namespace App\Http\Controllers\Orders;

use App\ABCorp\CWP\API\OrderItems;
use App\Http\Controllers\Controller;
use App\Http\Requests\PullActionsRequest;
use App\Traits\AdvancedSearch;
use Illuminate\Http\Request;

class OrderItemsController extends Controller
{
    use AdvancedSearch;

    private $orderItems;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orderItems = new OrderItems;

            return $next($request);
        });
    }

    /**
     * Get items list for the work order.
     *
     * @param string $work_order_uid
     * @param Request $request
     *
     * @return \Illuminate\View\View
     * @throws \Throwable
     */
    public function data(string $work_order_uid, Request $request)
    {
        $searchData = $this->orderItems->orderItems([
            'currentpage'  => 1,
            'itemsPerPage' => 0,
            'workOrderUid' => $work_order_uid,
        ]);

        $data = $this->orderItems->orderItems([
            'currentpage'  => $this->getCurrentPage($request),
            'workOrderUid' => $work_order_uid,
            'searchFields' => $this->getSearchFields($searchData->searchFields),
        ]);

        $data = $this->orderItems->getItemsActions($data);

        return $this->response('pages.orders.items', $request, [
            'data'           => $data,
            'include_search' => $this->isIncludeSearch($data->searchFields),
            'countries'      => $this->orderItems->getCountries(),
            'work_order_uid' => $work_order_uid,
        ]);
    }

    /**
     * Get details for the order item.
     *
     * @param string $id
     *
     * @return \Illuminate\View\View
     */
    public function show(string $id)
    {
        $details = $this->orderItems->itemDetails($id);

        if (!isset($details->data)) {
            return response()->json(['error' => $details, 'title' => 'Error'], 400);
        }

        return view('pages.orders.items.details.data', compact('details'));
    }

    /**
     * Make a pull request.
     *
     * @param PullActionsRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function action(PullActionsRequest $request)
    {
        $results = $this->orderItems->pullAction($request->all());

        if (is_array($results)) {
            return response()->json([
                'title'   => __('Pull request'),
                'message' => __('Your pull request has been successfully sent'),
            ]);
        }

        return response()->json(['error' => $results, 'title' => 'Error']);
    }

    /**
     * Get actions forms for item.
     *
     * @param string $item_uid
     * @param Request $request
     *
     * @return string
     * @throws \Throwable
     */
    public function actions(string $item_uid, Request $request)
    {
        $item_actions = $request->actions;

        $actions = $this->orderItems->getActions();

        return view('pages.orders.items.actions.tabs',
            compact('actions', 'item_actions', 'item_uid'))->render();
    }
}
