<?php

namespace App\Http\Controllers\ProductManagement;

use App\ABCorp\CWP\API\Product;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVertical;

class VerticalsController extends Controller
{
    private $product;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->product = new Product;

            return $next($request);
        });
    }

    public function index()
    {
        $result = $this->product->byCustomer();

        if (!isset($result->productVerticals)) {
            return response()->json("Failed to fetch products verticals!", 422);
        }

        $verticals = [];

        foreach ($result->productVerticals as $productVertical) {

            $products = [];

            if (count($productVertical->value->products)) {
                foreach ($productVertical->value->products as $product) {
                    $products[$product->uid] = $product;
                }
            }

            $verticals[$productVertical->key] = [
                'name' => $productVertical->value->name,
                'products' => $products
            ];
        }

        return $verticals;
    }

    public function store(StoreVertical $request)
    {
        $result = $this->product->addOrUpdateProductVertical([
            'productVertical' => [
                'name' => $request->vertical,
            ]
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json(true, 200);
        }

        return response()->json('Something goes wrong. Please Try again later!', 422);
    }


}
