<?php

namespace App\Http\Controllers\ProductManagement;

use App\ABCorp\CWP\API\Product;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductItem;

class ProductManagementController extends Controller
{
    private $product;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->product = new Product;

            return $next($request);
        });
    }

    public function index()
    {
        return view('pages.administration.product-management.index');
    }

    public function store(StoreProductItem $request)
    {
        $result = $this->product->addOrUpdateProductWithPart($request->all());

        if (isset($result->success) && $result->success) {
            return response()->json('The product item was successfully added.', 200);
        }

        return response()->json('Something goes wrong. Please Try again later!', 422);
    }
}
