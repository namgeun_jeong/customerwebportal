<?php

namespace App\Http\Controllers\ProductManagement;


use App\ABCorp\CWP\API\Product;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProduct;

class ProductsController extends Controller
{
    private $product;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->product = new Product;

            return $next($request);
        });
    }

    public function index()
    {

    }

    public function store(StoreProduct $request)
    {
        $result = $this->product->addOrUpdateProductWithPart([
            'prodVerticalUid' => $request->prodVerticalUid,
            'productWithPart' => [
                'part' => [
                    'uid' => '',
                    'name' => '',
                    'coID' => '',
                    'partID' => '',
                    'unitPrice' => 0,
                    'jobCopy' => '',
                    'engID' => '',
                    'engLotID' => '',
                ],
                'name' => $request->name,
                'dataPrepID' => $request->dataPrepId,
            ]
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json(true, 200);
        }

        return response()->json('Something goes wrong. Please Try again later!', 422);
    }
}
