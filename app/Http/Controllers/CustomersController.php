<?php

namespace App\Http\Controllers;

use App\ABCorp\CWP\API\Customer;
use Illuminate\Support\Facades\Cookie;

class CustomersController extends Controller
{
    private $customer;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->customer = new Customer();

            return $next($request);
        });
    }

    /**
     * Switch customer data and theme.
     *
     * @param $name
     * @return \Illuminate\Http\RedirectResponse
     */
    public function switchCustomer($name)
    {
        $customer = $this->customer->customerTheme($name);

        session([
            'customerName' => $name,
            'customerLogo' => $customer->logo ?? '',
        ]);

        Cookie::queue(cookie('customerName', $name));
        Cookie::queue(cookie('customerLogo', $customer->logo ?? ''));

        return redirect()->back();
    }
}
