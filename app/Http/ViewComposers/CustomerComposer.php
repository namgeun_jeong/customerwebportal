<?php

namespace App\Http\ViewComposers;

use App\ABCorp\CWP\API\Customer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use Illuminate\View\View;

class CustomerComposer
{
    private $customerName;

    private $customerLogo;

    private $timezone_offset;

    private $customers = [];

    public function __construct()
    {
        $customer = new Customer();

        $result = Cache::rememberForever('customers', function() use ($customer) {
            return $customer->customers();
        });

        if (!isset($result->customers) || !count((array)$result->customers)) {
            return false;
        }

        foreach ((array)$result->customers as $cust) {
            if (isset($cust->dataPrepID)) {
                $this->customers[$cust->dataPrepID] = $cust->name;
            }
        }

        if (session()->has('customerName')) {
            $this->customerName = session('customerName');
        } elseif (Cookie::get('customerName')) {
            $this->customerName = Cookie::get('customerName');
        } else {
            $this->customerName = array_keys($this->customers)[0];
        }

        $customerData = Cache::rememberForever('customerTheme', function() use ($customer) {
            return $customer->customerTheme($this->customerName);
        });

        $this->customerLogo = $customerData->logo ?? '';

        session([
            'customerName' => $this->customerName,
            'customerLogo' => $this->customerLogo,
        ]);

        cookie('customerName', $this->customerName);
        cookie('customerLogo', $this->customerLogo);

        if (session()->has('timezone_offset')) {
            $this->timezone_offset = session('timezone_offset');
        } else {
            $this->timezone_offset = 0;
        }
    }

    public function compose(View $view)
    {
        $view
            ->with('customers', $this->customers)
            ->with('customerName', $this->customerName)
            ->with('customerLogo', $this->customerLogo)
            ->with('timezone_offset', $this->timezone_offset);
    }
}