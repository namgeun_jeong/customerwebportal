<?php

namespace App\Http\ViewComposers;

use App\ABCorp\CWP\API\OrderItems;
use Illuminate\View\View;

class CountriesComposer
{
    protected $customer;

    public function __construct()
    {
        $this->orderItems = new OrderItems();
    }

    public function compose(View $view)
    {
        $view->with('countries', $this->orderItems->getCountries());
    }
}