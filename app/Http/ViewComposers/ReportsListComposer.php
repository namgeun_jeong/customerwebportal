<?php

namespace App\Http\ViewComposers;

use App\ABCorp\CWP\API\Reports;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class ReportsListComposer
{
    public function compose(View $view)
    {
        $reports = new Reports;

        // $reportsList =  Cache::rememberForever('reports', function() use ($reports) {
        //     return $reports->reports(true);
        // });
            
        $_roles =  auth()->user()->getUserInfo();
        $_role = $_roles['roles'][0];

        $reportsList = $reports->reports(true,$_role);
        $view->with('reports', $reportsList);
    }
}